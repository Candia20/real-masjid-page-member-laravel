<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController as Home;
use App\Http\Controllers\AuthPageController as AuthPage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [Home::class, 'index']);
Route::get('/list-kajian', [Home::class, 'listKajian']);
Route::get('/detail-kajian', [Home::class, 'detailKajian']);
Route::get('/play-video', [Home::class, 'playVideo']);
Route::get('/play-video2', [Home::class, 'playVideo2']);
Route::get('/login', [AuthPage::class, 'login']);
Route::get('/register', [AuthPage::class, 'register']);
Route::get('/reset-password', [AuthPage::class, 'resetPassword']);
Route::get('/set-password', [AuthPage::class, 'setPassword']);
Route::get('/verify', [AuthPage::class, 'verifyCode']);
Route::get('/verify-email', [AuthPage::class, 'verifyEmail']);

