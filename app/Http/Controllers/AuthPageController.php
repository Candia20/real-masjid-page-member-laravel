<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthPageController extends Controller
{
    public function login () {
        return view('auth/login');
    }

    public function register () {
        return view('auth/register');
    }

    public function verifyEmail () {
        return view('auth/VerifyEmail');
    }

    public function resetPassword () {
        return view('auth/ResetPassword');
    }

    public function setPassword () {
        return view('auth/setPassword');
    }

    public function verifyCode () {
        return view('auth/Verify');
    }


}
