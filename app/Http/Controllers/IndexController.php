<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index () {
        return view('IndexPage');
    }

    public function listKajian () {
        return view('ListKajianPage');
    }

    public function detailKajian () {
        return view('DetailKajian');
    }

    public function playVideo () {
        return view('PlayVideo');
    }

    public function playVideo2 () {
        return view('PlayVideo2');
    }
}
