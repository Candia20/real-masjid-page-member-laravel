        @section('content')
        <!-- Navbar -->
        <div class="container">
            <nav class="navbar navbar-expand-lg bg-nav text-dark">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#">
                        <img src="/image/logoRm.png" alt="Logaaao Real Masjid" width="30">
                    </a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="#">Home</a>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Dropdown
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="#">Action</a></li>
                                    <li><a class="dropdown-item" href="#">Another action</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                                </ul>
                            </li>

                            <!-- <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Kegiatan
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item dropdown">
                                        <a class="dropdown-item nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdownRA" aria-expanded="false">Real Academy</a>
                                        <ul>
                                            <li><a class="dropdown-item" href="#">Real Marbot Academy</a></li>
                                            <li><a class="dropdown-item" href="#">Real Marbot Preneur</a></li>
                                        </ul>
                                    </li>
                                    <li><a class="dropdown-item" href="#">Real Adventure</a></li>
                                    <li><a class="dropdown-item" href="#">Real Holiday</a></li>
                                    <li><a class="dropdown-item" href="#">SUFI (Suka Film)</a></li>
                                    <li><a class="dropdown-item" href="#">Pasar Raya Jumat</a></li>
                                    <li><a class="dropdown-item" href="#">Nobar</a></li>
                                    <li><a class="dropdown-item" href="#">Jogja Walk Around</a></li>
                                    <li><a class="dropdown-item" href="#">Real Qurban</a></li>
                                    <li><a class="dropdown-item" href="#">Turnament Pimpong</a></li>
                                    <li><a class="dropdown-item" href="#">Jogja Mengaji</a></li>
                                    <li><a class="dropdown-item" href="#">I'tikaf</a></li>
                                </ul>
                            </li> -->

                            <li class="nav-item">
                                <a class="nav-link" href="#">Infaq</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Kas Masjid</a>
                            </li>
                        </ul>
                        <button id="btn-utama" class="btn btn-utama" type="button" onclick="window.location.href='/login'">Masuk</button>
                    </div>
                </div>
            </nav>
        </div>
        <!-- End of Navbar -->

        @stop


