<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home Page</title>
    <link rel="icon" href="/image/logoRm.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="/owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="/owlcarousel/assets/owl.theme.default.min.css">
    <script src="https://kit.fontawesome.com/59f485dc95.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/bootstraplibrary/css/bootstrap.css">
    <link rel="stylesheet" href="/css/Index.css">
</head>

<body>

    <div class="main">
        <div class="container">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg bg-nav text-dark">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#">
                        <img src="/image/logoRm.png" alt="Logaaao Real Masjid" width="30">
                    </a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="#">Home</a>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    Dropdown
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="#">Action</a></li>
                                    <li><a class="dropdown-item" href="#">Another action</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                                </ul>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    Kegiatan
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item dropdown">
                                        <a class="dropdown-item nav-link dropdown-toggle" href="#" role="button"
                                            data-bs-toggle="dropdownRA" aria-expanded="false">Real
                                            Academy</a>
                                        <ul>
                                            <li><a class="dropdown-item" href="#">Real Marbot Academy</a></li>
                                            <li><a class="dropdown-item" href="#">Real Marbot Preneur</a></li>
                                        </ul>
                                    </li>
                                    <li><a class="dropdown-item" href="#">Real Adventure</a></li>
                                    <li><a class="dropdown-item" href="#">Real Holiday</a></li>
                                    <li><a class="dropdown-item" href="#">SUFI (Suka Film)</a></li>
                                    <li><a class="dropdown-item" href="#">Pasar Raya Jumat</a></li>
                                    <li><a class="dropdown-item" href="#">Nobar</a></li>
                                    <li><a class="dropdown-item" href="#">Jogja Walk Around</a></li>
                                    <li><a class="dropdown-item" href="#">Real Qurban</a></li>
                                    <li><a class="dropdown-item" href="#">Turnament Pimpong</a></li>
                                    <li><a class="dropdown-item" href="#">Jogja Mengaji</a></li>
                                    <li><a class="dropdown-item" href="#">I'tikaf</a></li>
                                </ul>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#">Infaq</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Kas Masjid</a>
                            </li>
                        </ul>
                        <button id="btn-utama" class="btn btn-utama" type="button"
                            onclick="window.location.href='/login'">Masuk</button>
                    </div>
                </div>
            </nav>
            <!-- End of Navbar -->

            <!-- Hero -->
            <section class="hero-section margin-section">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="bg-page bg-hero2"></div>
                        <h1 class="text-hero">Film Positif, Kajian, <br /><span class="text-color">Teman
                                Baru,</span>
                            <br> dan banyak lagi.
                        </h1>
                        <p class="subtxt-hero">Yuk mulai petualangan seru kamu di Real Masjid 2.0</p>
                        <button id="btn-utama" class="btn btn-utama">Gabung Sekarang</button>
                    </div>
                    <div class="col-md-6">
                        <div class="bg-page bg-hero"></div>
                        <img class="img-fluid img-hero" src="/image/img-hero.png" alt="gambar hero">
                    </div>
                </div>
            </section>
            <!-- End of Hero -->

            <!-- Buat Pengalaman di Real Masjid -->
            <section class="pengalaman margin-section">
                <div class="row align-items-center mb-3">
                    <div class="col-sm-12 col-md-6">
                        <h2 class="title-section">Buat Pengalaman Baru di Real Masjid 2.0</h2>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <p class="desc-section">Jadikan hari mu lebih meyenangkan dan berwarna dengan datang ke
                            <strong>REAL
                                MASJID</strong> yang
                            akan selalu
                            menghiburmu, karena hiburanmu tidak lagi di tempat hiburan.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="bg-page bg-pengalaman"></div>
                        <div id="carouselPengalaman" class="carousel slide" data-bs-ride="true">
                            <div class="carousel-indicators">
                                <button type="button" data-bs-target="#carouselPengalaman" data-bs-slide-to="0"
                                    class="active" aria-label="Slide 1"></button>
                                <button type="button" data-bs-target="#carouselPengalaman" data-bs-slide-to="1"
                                    aria-label="Slide 2"></button>
                                <button type="button" data-bs-target="#carouselPengalaman" data-bs-slide-to="2"
                                    aria-label="Slide 3"></button>
                            </div>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="/image/img-pengalaman1.png" alt="pengalaman1" class="img-fluid">
                                </div>
                                {{-- <div class="carousel-item">
                                    <img src="/image/img-pengalaman2.jpeg" alt="pengalaman2" class="d-block w-100">
                                </div>
                                <div class="carousel-item">
                                    <img src="/image/img-pengalaman3.jpg" alt="pengalaman3" class="d-block w-100">
                                </div> --}}
                                <button class="carousel-control-prev" type="button"
                                    data-bs-target="#carouselPengalaman">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Previous</span>
                                </button>
                                <button class="carousel-control-next" type="button"
                                    data-bs-target="#carouselPengalaman">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Next</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row pt-3">
                        <div class="col-sm-6 col-md-3">
                            <div class="row">
                                <div class="col-1">
                                    <i class="fa-regular fa-circle-check" style="color: #8ee06b;"></i>
                                </div>
                                <div class="col-10 py-0">
                                    <p>Selalu dapat kenalan teman baru yang asik</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="row">
                                <div class="col-1">
                                    <i class="fa-regular fa-circle-check" style="color: #8ee06b;"></i>
                                </div>
                                <div class="col-10 py-0">
                                    <p>Dapat teman yang saling menguatkan dalam kebaikan</p>
                                </div>
                            </div>
                        </div>
                        <div class=" col-sm-6 col-md-3">
                            <div class="row">
                                <div class="col-1">
                                    <i class="fa-regular fa-circle-check" style="color: #8ee06b;"></i>
                                </div>
                                <div class="col-10 py-0">
                                    <p>Hiburan yang sangat seru dan berpahala</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="row">
                                <div class="col-1">
                                    <i class="fa-regular fa-circle-check" style="color: #8ee06b;"></i>
                                </div>
                                <div class="col-10 py-0">
                                    <p>Pengalaman yang berharga di Real Masjid</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Buat Pengalaman di Real Masjid -->

            <!-- Kajian Terbaru -->
            <section class="kajian-terbaru margin-section">
                <div class="row text-center">
                    <div class="col-md-12">
                        <div class="bg-page bg-jadwal1"></div>
                        <div class="bg-page bg-jadwal2"></div>
                        <h2 class="title-section">Kajian Terbaru</h2>
                        <p class="desc-section mb-5">Yuk terus belajar memperbanyak ilmu agama islam di Real Masjid
                        </p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-6 col-sm-4 col-md-3">
                        <div class="card">
                            <img src="/image/img-jadwal-kajian1.jpg" class="card-img-top" alt="jadwal kajian1">
                            <div class="card-body">
                                <div>
                                    <p class="card-title">Kajian Subuh Akbar Pasar Raya Jumat - Kyai Jahir</p>
                                </div>
                                <div>
                                    <small class="card-text">
                                        <i class="fa-regular fa-calendar-check" style="color: #c88441;"></i>
                                        <span>15 Agustus 2022</span>
                                    </small>
                                </div>
                                <div>
                                    <small class="card-text">
                                        <i class="fa-regular fa-clock fa-spin" style="color: #c88441;"></i>
                                        <span>15:30 WIB</span>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-3">
                        <div class="card">
                            <img src="/image/img-jadwal-kajian2.jpg" class="card-img-top" alt="jadwal kajian1">
                            <div class="card-body">
                                <div>
                                    <p class="card-title">Kajian Subuh - Kyai Jahir</p>
                                </div>
                                <div>
                                    <small class="card-text">
                                        <i class="fa-regular fa-calendar-check" style="color: #c88441;"></i>
                                        <span>15 Agustus 2022</span>
                                    </small>
                                </div>
                                <div>
                                    <small class="card-text">
                                        <i class="fa-regular fa-clock fa-spin" style="color: #c88441;"></i>
                                        <span>15:30 WIB</span>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-3">
                        <div class="card">
                            <img src="/image/img-jadwal-kajian3.jpg" class="card-img-top" alt="jadwal kajian1">
                            <div class="card-body">
                                <div>
                                    <p class="card-title">Kajian Subuh - Kyai Jahir</p>
                                </div>
                                <div>
                                    <small class="card-text">
                                        <i class="fa-regular fa-calendar-check" style="color: #c88441;"></i>
                                        <span>15 Agustus 2022</span>
                                    </small>
                                </div>
                                <div>
                                    <small class="card-text">
                                        <i class="fa-regular fa-clock fa-spin" style="color: #c88441;"></i>
                                        <span>15:30 WIB</span>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-3">
                        <div class="card">
                            <img src="/image/img-jadwal-kajian4.jpg" class="card-img-top" alt="jadwal kajian1">
                            <div class="card-body">
                                <div>
                                    <p class="card-title">Kajian Subuh - Kyai Jahir</p>
                                </div>
                                <div>
                                    <small class="card-text">
                                        <i class="fa-regular fa-calendar-check" style="color: #c88441;"></i>
                                        <span>15 Agustus 2022</span>
                                    </small>
                                </div>
                                <div>
                                    <small class="card-text">
                                        <i class="fa-regular fa-clock fa-spin" style="color: #c88441;"></i>
                                        <span>15:30 WIB</span>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-3">
                        <div class="card">
                            <img src="/image/img-jadwal-kajian5.jpg" class="card-img-top" alt="jadwal kajian1">
                            <div class="card-body">
                                <div>
                                    <p class="card-title">Kajian Subuh - Kyai Jahir</p>
                                </div>
                                <div>
                                    <small class="card-text">
                                        <i class="fa-regular fa-calendar-check" style="color: #c88441;"></i>
                                        <span>15 Agustus 2022</span>
                                    </small>
                                </div>
                                <div>
                                    <small class="card-text">
                                        <i class="fa-regular fa-clock fa-spin" style="color: #c88441;"></i>
                                        <span>15:30 WIB</span>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-3">
                        <div class="card">
                            <img src="/image/img-jadwal-kajian6.jpg" class="card-img-top" alt="jadwal kajian1">
                            <div class="card-body">
                                <div>
                                    <p class="card-title">Kajian Subuh - Kyai Jahir</p>
                                </div>
                                <div>
                                    <small class="card-text">
                                        <i class="fa-regular fa-calendar-check" style="color: #c88441;"></i>
                                        <span>15 Agustus 2022</span>
                                    </small>
                                </div>
                                <div>
                                    <small class="card-text">
                                        <i class="fa-regular fa-clock fa-spin" style="color: #c88441;"></i>
                                        <span>15:30 WIB</span>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-3">
                        <div class="card">
                            <img src="/image/img-jadwal-kajian5.jpg" class="card-img-top" alt="jadwal kajian1">
                            <div class="card-body">
                                <div>
                                    <p class="card-title">Kajian Subuh - Kyai Jahir</p>
                                </div>
                                <div>
                                    <small class="card-text">
                                        <i class="fa-regular fa-calendar-check" style="color: #c88441;"></i>
                                        <span>15 Agustus 2022</span>
                                    </small>
                                </div>
                                <div>
                                    <small class="card-text">
                                        <i class="fa-regular fa-clock fa-spin" style="color: #c88441;"></i>
                                        <span>15:30 WIB</span>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-3">
                        <div class="card">
                            <img src="/image/img-jadwal-kajian6.jpg" class="card-img-top" alt="jadwal kajian1">
                            <div class="card-body">
                                <div>
                                    <p class="card-title">Kajian Subuh - Kyai Jahir</p>
                                </div>
                                <div>
                                    <small class="card-text">
                                        <i class="fa-regular fa-calendar-check" style="color: #c88441;"></i>
                                        <span>15 Agustus 2022</span>
                                    </small>
                                </div>
                                <div>
                                    <small class="card-text">
                                        <i class="fa-regular fa-clock fa-spin" style="color: #c88441;"></i>
                                        <span>15:30 WIB</span>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center mt-5">
                    <button id="btn-utama" class="btn btn-utama" type="button"
                        onclick="window.location.href='/list-kajian'">Selengkapnya</button>
                </div>
            </section>
            <!-- End Kajian Terbaru -->

            <!-- Kegiatan Masjid -->
            {{-- <section class="kegiatan-masjid margin-section">
            <h2 class="title-section text-center mb-5">Kegiatan Masjid</h2>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 desc-kegiatan">
                        <h4>Jogja Walk Around</h4>
                        <p class="mb-3">Jalan kaki keliling Ring Road Yogyakarta sepanjang 40 KM, sambil melakukan
                            muhasabah akhir
                            tahun islam dan dzikir sepanjang perjalanan. Yuk tantang dirimu push the limit. </p>
                        <i class="icon-color bi bi-calendar-week-fill"></i><span class="ps-2 pe-4">15 Agustus
                            2022</span>
                        <i class="icon-color bi bi-clock-fill"></i><span class="ps-2 pe-4">15:30 WIB</span>
                        <i class="icon-color bi bi-people-fill"></i><span class="ps-2">Peserta Terbatas</span>
                        <img class="img-kegiatan" src="/image/img-kegiatan1.jpg" alt="">
                    </div>
                    <div class="col-md-6 d-flex align-items-center justify-content-center">
                        <ul class="list-kegiatan">
                            <li class="item-kegiatan">
                                <div class="bg-rounded d-flex align-items-center justify-content-center">
                                    <span class="no-kegiatan">1</span>
                                </div>
                                <div class="desc-kegiatan">
                                    <h3>Real Masjid Berholawat</h3>
                                    <p>Melakukan baca Al-Qur’an serentak di Malioboro.</p>
                                </div>
                            </li>
                            <li class="item-kegiatan">
                                <div class="bg-rounded d-flex align-items-center justify-content-center">
                                    <span class="no-kegiatan">2</span>
                                </div>
                                <div class="desc-kegiatan">
                                    <h3>Marbot Show</h3>
                                    <p>Melakukan baca Al-Qur’an serentak di Malioboro.</p>
                                </div>
                            </li>
                            <li class="item-kegiatan">
                                <div class="bg-rounded d-flex align-items-center justify-content-center">
                                    <span class="no-kegiatan">3</span>
                                </div>
                                <div class="desc-kegiatan">
                                    <h3>Senyum Manis</h3>
                                    <p>Melakukan baca Al-Qur’an serentak di Malioboro.</p>
                                </div>
                            </li>
                            <li class="item-kegiatan">
                                <div class="bg-rounded d-flex align-items-center justify-content-center">
                                    <span class="no-kegiatan">4</span>
                                </div>
                                <div class="desc-kegiatan">
                                    <h3>Nobar Timnas Final</h3>
                                    <p>Melakukan baca Al-Qur’an serentak di Malioboro.</p>
                                </div>
                            </li>
                            <li class="item-kegiatan">
                                <div class="bg-rounded d-flex align-items-center justify-content-center selected">
                                    <span class="no-kegiatan on-active">5</span>
                                </div>
                                <div class="desc-kegiatan selected-font">
                                    <h3>Jogja Walk Around</h3>
                                    <p>Jalan kaki keliling Ring Road Yogyakarta sepanjang 40 KM, Yuk tantang dirimu push
                                        the limit.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section> --}}
            <!-- End of Kegiatan Masjid -->

            <!-- Program Masjid -->
            <section class="program-masjid margin-section">
                <div class="bg-page bg-program"></div>
                <div class="row align-items-center mb-5">
                    <div class="col-md-6">
                        <h2 class="title-section">Program Masjid</h2>
                    </div>
                    <div class="col-md-6">
                        <p class="desc-section">Jadikan hari mu lebih meyenangkan dan berwarna dengan datang ke
                            <strong>REAL MASJID</strong> yang akan selalu menghiburmu, karena hiburanmu tidak lagi
                            di
                            tempat hiburan.
                        </p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-6 col-sm-4 col-md-2">
                        <div class="card">
                            <img class="img-program img-fluid" src="/image/img-program1.jpg" alt="Program1">
                            <div id="card-body" class="card-body text-center">
                                <h6 class="mb-0">Subuh Berhadiah</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2">
                        <div class="card">
                            <img class="img-program img-fluid" src="/image/img-program2.jpg" alt="Program1">
                            <div id="card-body" class="card-body text-center">
                                <h6 class="mb-0">Kajian Sunrise</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2">
                        <div class="card">
                            <img class="img-program img-fluid" src="/image/img-program3.jpg" alt="Program1">
                            <div id="card-body" class="card-body text-center">
                                <h6 class="mb-0">Tahajud Al-Kahfi</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2">
                        <div class="card">
                            <img class="img-program img-fluid" src="/image/img-program4.jpg" alt="Program1">
                            <div id="card-body" class="card-body text-center">
                                <h6 class="mb-0">Dzikir Pagi-Sore</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2">
                        <div class="card">
                            <img class="img-program img-fluid" src="/image/img-program5.jpg" alt="Program1">
                            <div id="card-body" class="card-body text-center">
                                <h6 class="mb-0">One Day One Juz</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2">
                        <div class="card">
                            <img class="img-program img-fluid" src="/image/img-program1.jpg" alt="Program1">
                            <div id="card-body" class="card-body text-center">
                                <h6 class="mb-0">Menghafal itu Mudah (MIM)</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2">
                        <div class="card">
                            <img class="img-program img-fluid" src="/image/img-program2.jpg" alt="Program1">
                            <div id="card-body" class="card-body text-center">
                                <h6 class="mb-0">Festival Anak Sholeh</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2">
                        <div class="card">
                            <img class="img-program img-fluid" src="/image/img-program3.jpg" alt="Program1">
                            <div id="card-body" class="card-body text-center">
                                <h6 class="mb-0">Real Masjid Bersholawat</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2">
                        <div class="card">
                            <img class="img-program img-fluid" src="/image/img-program4.jpg" alt="Program1">
                            <div id="card-body" class="card-body text-center">
                                <h6 class="mb-0">GUSBAHA</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2">
                        <div class="card">
                            <img class="img-program img-fluid" src="/image/img-program5.jpg" alt="Program1">
                            <div id="card-body" class="card-body text-center">
                                <h6 class="mb-0">Marbot Pride</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2">
                        <div class="card">
                            <img class="img-program img-fluid" src="/image/img-program1.jpg" alt="Program1">
                            <div id="card-body" class="card-body text-center">
                                <h6 class="mb-0">Peduli Yatim & Duafa</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2">
                        <div class="card">
                            <img class="img-program img-fluid" src="/image/img-program2.jpg" alt="Program1">
                            <div id="card-body" class="card-body text-center">
                                <h6 class="mb-0">Jogja Mengaji</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2">
                        <div class="card">
                            <img class="img-program img-fluid" src="/image/img-program3.jpg" alt="Program1">
                            <div id="card-body" class="card-body text-center">
                                <h6 class="mb-0">Real Ramdhan</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2">
                        <div class="card">
                            <img class="img-program img-fluid" src="/image/img-program4.jpg" alt="Program1">
                            <div id="card-body" class="card-body text-center">
                                <h6 class="mb-0">Real Qurban</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2">
                        <div class="card">
                            <img class="img-program img-fluid" src="/image/img-program5.jpg" alt="Program1">
                            <div id="card-body" class="card-body text-center">
                                <h6 class="mb-0">Sedekah Subuh</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2">
                        <div class="card">
                            <img class="img-program img-fluid" src="/image/img-program5.jpg" alt="Program1">
                            <div id="card-body" class="card-body text-center">
                                <h6 class="mb-0">Real Marbot Academy</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2">
                        <div class="card">
                            <img class="img-program img-fluid" src="/image/img-program5.jpg" alt="Program1">
                            <div id="card-body" class="card-body text-center">
                                <h6 class="mb-0">Real Marbot Preneur</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2">
                        <div class="card">
                            <img class="img-program img-fluid" src="/image/img-program5.jpg" alt="Program1">
                            <div id="card-body" class="card-body text-center">
                                <h6 class="mb-0">Real Holiday</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Layanan Masjid -->
            <section class="layanan-masjid margin-section">
                <div class="container">
                    <div class="row align-items-center mb-5">
                        <div class="col-md-6">
                            <h2 class="title-section">Layanan Masjid</h2>
                        </div>
                        <div class="col-md-6">
                            <p class="desc-section">Jadikan hari mu lebih meyenangkan dan berwarna dengan datang ke
                                <strong>REAL MASJID</strong> yang akan selalu menghiburmu, karena hiburanmu tidak lagi
                                di
                                tempat hiburan.
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 col-sm-4 col-md-3 mb-3">
                            <div class="bg-rounded d-flex align-items-center justify-content-center selected mb-3">
                                <span class="no-kegiatan on-active">1</span>
                            </div>
                            <div class="desc-kegiatan selected-font">
                                <h6><strong>Makan Gratis</strong></h6>
                                <p>Makan gratis 3x sehari. Setiap pagi, ba'da dzuhur, dan ba'da maghrib.</p>
                            </div>
                        </div>
                        <div class="col-6 col-sm-4 col-md-3 mb-3">
                            <div class="bg-rounded d-flex align-items-center justify-content-center selected mb-3">
                                <span class="no-kegiatan on-active">2</span>
                            </div>
                            <div class="desc-kegiatan selected-font">
                                <h6><strong>Sarung, Mukena, dan Rosal</strong></h6>
                                <p>Tersedia alat sholat untuk jama’ah yang tidak membawa.</p>
                            </div>
                        </div>
                        <div class="col-6 col-sm-4 col-md-3 mb-3">
                            <div class="bg-rounded d-flex align-items-center justify-content-center selected mb-3">
                                <span class="no-kegiatan on-active">3</span>
                            </div>
                            <div class="desc-kegiatan selected-font">
                                <h6><strong>Santri Fii Sabilillah</strong></h6>
                                <p>Menerima relawan ingin berkontribusi dalam program real masjid.</p>
                            </div>
                        </div>
                        <div class="col-6 col-sm-4 col-md-3 mb-3">
                            <div class="bg-rounded d-flex align-items-center justify-content-center selected mb-3">
                                <span class="no-kegiatan on-active">4</span>
                            </div>
                            <div class="desc-kegiatan selected-font">
                                <h6><strong>Santri Pengemban Dakwah</strong></h6>
                                <p>Bagi santri fii sabilillah yang berprestasi, ada kesempatan untuk bergabung menjadi
                                    SPD.
                                </p>
                            </div>
                        </div>
                        <div class="col-6 col-sm-4 col-md-3 mb-3">
                            <div class="bg-rounded d-flex align-items-center justify-content-center selected mb-3">
                                <span class="no-kegiatan on-active">5</span>
                            </div>
                            <div class="desc-kegiatan selected-font">
                                <h6><strong>Makan Gratis</strong></h6>
                                <p>Makan gratis 3x sehari. Setiap pagi, ba'da dzuhur, dan ba'da maghrib.</p>
                            </div>
                        </div>
                        <div class="col-6 col-sm-4 col-md-3 mb-3">
                            <div class="bg-rounded d-flex align-items-center justify-content-center selected mb-3">
                                <span class="no-kegiatan on-active">6</span>
                            </div>
                            <div class="desc-kegiatan selected-font">
                                <h6><strong>Sarung, Mukena, dan Rosal</strong></h6>
                                <p>Tersedia alat sholat untuk jama’ah yang tidak membawa.</p>
                            </div>
                        </div>
                        <div class="col-6 col-sm-4 col-md-3 mb-3">
                            <div class="bg-rounded d-flex align-items-center justify-content-center selected mb-3">
                                <span class="no-kegiatan on-active">7</span>
                            </div>
                            <div class="desc-kegiatan selected-font">
                                <h6><strong>Santri Fii Sabilillah</strong></h6>
                                <p>Menerima relawan ingin berkontribusi dalam program real masjid.</p>
                            </div>
                        </div>
                        <div class="col-6 col-sm-4 col-md-3 mb-3">
                            <div class="bg-rounded d-flex align-items-center justify-content-center selected mb-3">
                                <span class="no-kegiatan on-active">8</span>
                            </div>
                            <div class="desc-kegiatan selected-font">
                                <h6><strong>Santri Pengemban Dakwah</strong></h6>
                                <p>Bagi santri fii sabilillah yang berprestasi, ada kesempatan untuk bergabung menjadi
                                    SPD.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End of Layanan Masjid -->

            <!-- Fasilitas Masjid -->
            <section class="fasilitas-masjid text-center margin-section">
                <div class="bg-page bg-fasilitas"></div>
                <h2 class="title-section mb-5">Fasilitas Masjid</h2>
                <div class="row justify-content-center">
                    <div class="col-4 col-sm-3 col-md-2 icon-fasilitas">
                        <i class="fa-solid fa-wifi fa-2xl" style="color: #87471C;"></i>
                        <p class="mt-3 text-fasilitas">Internet Gratis</p>
                    </div>
                    <div class="col-4 col-sm-3 col-md-2 icon-fasilitas">
                        <i class="fa-solid fa-book-open fa-2xl" style="color: #87471C;"></i>
                        <p class="mt-3 text-fasilitas">Perpustakaan</p>
                    </div>
                    <div class="col-4 col-sm-3 col-md-2 icon-fasilitas">
                        <i class="fa-sharp fa-solid fa-building fa-2xl" style="color: #87471C;"></i>
                        <p class="mt-3 text-fasilitas">Ruang Serba Guna</p>
                    </div>
                    <div class="col-4 col-sm-3 col-md-2 icon-fasilitas">
                        <i class="fa-solid fa-spray-can-sparkles fa-2xl" style="color: #87471C;"></i>
                        <p class="mt-3 text-fasilitas">Parfum Jama’ah</p>
                    </div>
                    <div class="col-4 col-sm-3 col-md-2 icon-fasilitas">
                        <i class="fa-sharp fa-solid fa-tent fa-2xl" style="color: #87471C;"></i>
                        <p class="mt-3 text-fasilitas">Tenda Musafir</p>
                    </div>
                    <div class="col-4 col-sm-3 col-md-2 icon-fasilitas">
                        <i class="fa-solid fa-video fa-2xl" style="color: #87471C;"></i>
                        <p class="mt-3 text-fasilitas">Biskop Masjid</p>
                    </div>
                    <div class="col-4 col-sm-3 col-md-2 icon-fasilitas">
                        <i class="fa-sharp fa-solid fa-bottle-water fa-2xl" style="color: #87471C;"></i>
                        <p class="mt-3 text-fasilitas">Air Sehat untuk Santri</p>
                    </div>
                    <div class="col-4 col-sm-3 col-md-2 icon-fasilitas">
                        <i class="fa-solid fa-square-parking fa-2xl" style="color: #87471C;"></i>
                        <p class="mt-3 text-fasilitas">Parkir</p>
                    </div>
                    <div class="col-4 col-sm-3 col-md-2 icon-fasilitas">
                        <i class="fa-solid fa-children fa-2xl" style="color: #87471C;"></i>
                        <p class="mt-3 text-fasilitas">Ramah Anak</p>
                    </div>
                    <div class="col-4 col-sm-3 col-md-2 icon-fasilitas">
                        <i class="fa-solid fa-person fa-2xl" style="color: #87471C;"></i>
                        <p class="mt-3 text-fasilitas">Ramah Pemuda</p>
                    </div>
                    <div class="col-4 col-sm-3 col-md-2 icon-fasilitas">
                        <i class="fa-solid fa-graduation-cap fa-2xl" style="color: #87471C;"></i>
                        <p class="mt-3 text-fasilitas">Pondok Pesantren</p>
                    </div>
                    <div class="col-4 col-sm-3 col-md-2 icon-fasilitas">
                        <i class="fa-solid fa-basketball fa-2xl" style="color: #87471C;"></i>
                        <p class="mt-3 text-fasilitas">Sport Area</p>
                    </div>
                    <div class="col-4 col-sm-3 col-md-2 icon-fasilitas">
                        <i class="fa-solid fa-shop fa-2xl" style="color: #87471C;"></i>
                        <p class="mt-3 text-fasilitas">Real Masjid Store</p>
                    </div>
                    <div class="col-4 col-sm-3 col-md-2 icon-fasilitas">
                        <i class="fa-solid fa-shop fa-2xl" style="color: #87471C;"></i>
                        <p class="mt-3 text-fasilitas">Ruang Tamu VIP</p>
                    </div>
                    <div class="col-4 col-sm-3 col-md-2 icon-fasilitas">
                        <i class="fa-solid fa-shop fa-2xl" style="color: #87471C;"></i>
                        <p class="mt-3 text-fasilitas">Gazebo</p>
                    </div>
                    <div class="col-4 col-sm-3 col-md-2 icon-fasilitas">
                        <i class="fa-solid fa-shop fa-2xl" style="color: #87471C;"></i>
                        <p class="mt-3 text-fasilitas">Gratis Makanan 3x Sehari</p>
                    </div>
                </div>
                <div class="desc-section">
                    <p class="w-50 mt-2 ms-auto me-auto">Jadikan hari mu lebih meyenangkan dan berwarna dengan
                        datang ke <strong>REAL MASJID</strong> yang akan selalu menghiburmu, karena hiburanmu tidak
                        lagi
                        di tempat
                        hiburan.</p>
                </div>
            </section>
            <!-- End of Fasilitas Masjid -->

            <!-- Kata Mereka Tentang Real Masjid -->
            <section class="kata-jamaah  margin-section">
                <h2 class="title-section text-center mb-5">Kata Mereka Tentang Real Masjid 2.0</h2>
                <div class="d-flex justify-content-end">
                    <div id="owl-carousel" class="owl-carousel slider-testimoni owl-theme mt-4">
                        <div class="item">
                            <div class="card-testimoni">
                                <p class="bg-nama">AHMAD <br> FAUZI</p>
                                <div class="card-img-overlay">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <img class="img-person" src="/image/person1.png" alt="">
                                        </div>
                                        <div class="col-md-7">
                                            <img class="icon-quote" src="/image/petik-dua.png" alt="">
                                            <p class="text-testimoni">Real Masjid bikin candu, awalnya cuman coba-coba
                                                lama-lama kok enak,
                                                mulai
                                                dari suasana masjid hingga marbotnya semua friendly.</p>
                                            <p class="nama-jamaah">Ahmad Fauzi</p>
                                            <p class="peran-jamaah">Jamaah</p>
                                            <div class="bg-page bg-card-testimoni"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-testimoni">
                                <p class="bg-nama">AHMAD <br> FAUZI</p>
                                <div class="card-img-overlay">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <img class="img-person" src="/image/person1.png" alt="">
                                        </div>
                                        <div class="col-md-7">
                                            <img class="icon-quote" src="/image/petik-dua.png" alt="">
                                            <p class="text-testimoni">Real Masjid bikin candu, awalnya cuman coba-coba
                                                lama-lama kok enak,
                                                mulai
                                                dari suasana masjid hingga marbotnya semua friendly.</p>
                                            <p class="nama-jamaah">Ahmad Fauzi</p>
                                            <p class="peran-jamaah">Jamaah</p>
                                            <div class="bg-page bg-card-testimoni"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-testimoni">
                                <p class="bg-nama">AHMAD <br> FAUZI</p>
                                <div class="card-img-overlay">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <img class="img-person" src="/image/person1.png" alt="">
                                        </div>
                                        <div class="col-md-7">
                                            <img class="icon-quote" src="/image/petik-dua.png" alt="">
                                            <p class="text-testimoni">Real Masjid bikin candu, awalnya cuman coba-coba
                                                lama-lama kok enak,
                                                mulai
                                                dari suasana masjid hingga marbotnya semua friendly.</p>
                                            <p class="nama-jamaah">Ahmad Fauzi</p>
                                            <p class="peran-jamaah">Jamaah</p>
                                            <div class="bg-page bg-card-testimoni"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-testimoni">
                                <p class="bg-nama">AHMAD <br> FAUZI</p>
                                <div class="card-img-overlay">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <img class="img-person" src="/image/person1.png" alt="">
                                        </div>
                                        <div class="col-md-7">
                                            <img class="icon-quote" src="/image/petik-dua.png" alt="">
                                            <p class="text-testimoni">Real Masjid bikin candu, awalnya cuman coba-coba
                                                lama-lama kok enak,
                                                mulai
                                                dari suasana masjid hingga marbotnya semua friendly.</p>
                                            <p class="nama-jamaah">Ahmad Fauzi</p>
                                            <p class="peran-jamaah">Jamaah</p>
                                            <div class="bg-page bg-card-testimoni"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-testimoni">
                                <p class="bg-nama">AHMAD <br> FAUZI</p>
                                <div class="card-img-overlay">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <img class="img-person" src="/image/person1.png" alt="">
                                        </div>
                                        <div class="col-md-7">
                                            <img class="icon-quote" src="/image/petik-dua.png" alt="">
                                            <p class="text-testimoni">Real Masjid bikin candu, awalnya cuman coba-coba
                                                lama-lama kok enak,
                                                mulai
                                                dari suasana masjid hingga marbotnya semua friendly.</p>
                                            <p class="nama-jamaah">Ahmad Fauzi</p>
                                            <p class="peran-jamaah">Jamaah</p>
                                            <div class="bg-page bg-card-testimoni"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End of Kata Mereka Tentang Real Masjid -->

            <!-- FAQ -->
            <section class="faq margin-section">
                <div class="container">
                    <div class="row text-center">
                        <h2 class="title-section">Pertanyaan Tentang Real Masjid 2.0</h2>
                        <p class="desc-section mb-5">Kumpulan pertanyaanmu ada disini ya! Semoga Menjawab.</p>
                    </div>
                    <div class="list-faq ms-auto me-auto">
                        <div class="accordion" id="accordionExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseOne" aria-expanded="true"
                                        aria-controls="collapseOne">
                                        Dimana sih lokasi Real Masjid 2.0?
                                    </button>
                                </h2>
                                <div id="collapseOne" class="accordion-collapse collapse show"
                                    aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <strong>This is the first item's accordion body.</strong> It is shown by
                                        default,
                                        until the collapse plugin adds the appropriate classes that we use to style each
                                        element. These classes control the overall appearance, as well as the showing
                                        and
                                        hiding via CSS transitions. You can modify any of this with custom CSS or
                                        overriding
                                        our default variables. It's also worth noting that just about any HTML can go
                                        within
                                        the <code>.accordion-body</code>, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button"
                                        data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false"
                                        aria-controls="collapseTwo">
                                        Ada berapa jumlah marbot di Real Masjid 2.0?
                                    </button>
                                </h2>
                                <div id="collapseTwo" class="accordion-collapse collapse"
                                    aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <strong>This is the second item's accordion body.</strong> It is hidden by
                                        default,
                                        until the collapse plugin adds the appropriate classes that we use to style each
                                        element. These classes control the overall appearance, as well as the showing
                                        and
                                        hiding via CSS transitions. You can modify any of this with custom CSS or
                                        overriding
                                        our default variables. It's also worth noting that just about any HTML can go
                                        within
                                        the <code>.accordion-body</code>, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingThree">
                                    <button class="accordion-button collapsed" type="button"
                                        data-bs-toggle="collapse" data-bs-target="#collapseThree"
                                        aria-expanded="false" aria-controls="collapseThree">
                                        Gimana cara daftar kajian dari website Real Masjid 2.0?
                                    </button>
                                </h2>
                                <div id="collapseThree" class="accordion-collapse collapse"
                                    aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <strong>This is the third item's accordion body.</strong> It is hidden by
                                        default,
                                        until the collapse plugin adds the appropriate classes that we use to style each
                                        element. These classes control the overall appearance, as well as the showing
                                        and
                                        hiding via CSS transitions. You can modify any of this with custom CSS or
                                        overriding
                                        our default variables. It's also worth noting that just about any HTML can go
                                        within
                                        the <code>.accordion-body</code>, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingFour">
                                    <button class="accordion-button collapsed" type="button"
                                        data-bs-toggle="collapse" data-bs-target="#collapseFour"
                                        aria-expanded="false" aria-controls="collapseFour">
                                        Gimana cara nonton kajian di website Real Masjid 2.0?
                                    </button>
                                </h2>
                                <div id="collapseFour" class="accordion-collapse collapse"
                                    aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        Pertama-tama harus buat akun dulu ya di website real masjid, setelah memiliki
                                        akun
                                        coba login dan pilih kajian yang ada. Taaraa Selamat Menonton!!
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <!-- End of FAQ -->

            <!-- Footer -->
            <footer class="footer py-5 my-5 border-top">
                <div class="row">
                    <img class="logo-footer mb-3" src="/image/logoRm2.png" />
                    <div class="col-12 col-sm-6 col-md-2 offside-md-2 mb-3">
                        <h6>Seputar Real Masjid</h6>
                        <ul class="nav flex-column">
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Marbot
                                    Pride</a>
                            </li>
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Kontributor
                                    Masjid</a>
                            </li>
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Lowongan
                                    Kerja</a></li>
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Tentang
                                    Real
                                    Masjid</a>
                            </li>
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Website
                                    Feedback</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-md-2 mb-3">
                        <h6>Real Masjid Academy</h6>
                        <ul class="nav flex-column">
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Real
                                    Marbot
                                    Academy</a>
                            </li>
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Real
                                    Marbot
                                    Preneur</a>
                            </li>
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Pesantren</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-md-2 mb-3">
                        <h6>Kontak Kami</h6>
                        <ul class="nav flex-column">
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">
                                    <i class="fa-solid fa-phone fa-md" style="color: #87471C;"></i>
                                    0851-5515-0511</a>
                            </li>
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted"><i
                                        class="fa-brands fa-whatsapp fa-md" style="color: #87471C;"></i>
                                    0851-5515-0511</a></li>
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted"><i
                                        class="fa-solid fa-envelope fa-md" style="color: #87471C;"></i>
                                    marbot@realmasjid.com</a></li>
                            <ul class="list-unstyled d-flex">
                                <li class="sosmed">
                                    <a href="#"><i class="fa-brands fa-facebook fa-xl"
                                            style="color: #87471c;"></i></a>
                                </li>
                                <li class="sosmed">
                                    <a href=" #"><i class="fa-brands fa-instagram fa-xl"
                                            style="color: #87471c;"></i></a>
                                </li>
                                <li class="sosmed">
                                    <a href="#"><i class="fa-brands fa-tiktok fa-xl"
                                            style="color: #87471c;"></i></a>
                                </li>
                                <li class="sosmed">
                                    <a href="#"><i class="fa-brands fa-twitter fa-xl"
                                            style="color: #87471c;"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa-brands fa-youtube fa-xl"
                                            style="color: #87471c;"></i></a>
                                </li>
                            </ul>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 mb-3 alamatrm">
                        <h6>Alamat</h6>
                        <p class="alamat">Jl. Ring Road Utara No.17, Candok, Condongcatur, Kec. Depok, Kabupaten
                            Sleman,
                            Daerah Istimewa Yogyakarta 55281</p>
                        {{-- <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.2886850556624!2d110.389554014154!3d-7.759176879096815!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a59db132d329f%3A0x77084b6e78c1a140!2sMasjid%20Muslim%20United!5e0!3m2!1sid!2sid!4v1666186510008!5m2!1sid!2sid"
                            width="352" height="200" style="border: 0" allowfullscreen="" loading="lazy"
                            referrerpolicy="no-referrer-when-downgrade"></iframe> --}}
                        <div class="mapouter">
                            <div class="gmap_canvas"><iframe width="100%" height="100%" id="gmap_canvas"
                                    src="https://maps.google.com/maps?q=real masjid&t=&z=12&ie=UTF8&iwloc=&output=embed"
                                    frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a
                                    href="https://2yu.co">2yu</a><br>
                                <style>
                                    .mapouter {
                                        position: relative;
                                        text-align: right;
                                        height: 75%;
                                        width: 100%;
                                    }
                                </style><a href="https://embedgooglemap.2yu.co/">html embed google map</a>
                                <style>
                                    .gmap_canvas {
                                        overflow: hidden;
                                        background: none !important;
                                        height: 100%;
                                        width: 100%;
                                    }
                                </style>
                            </div>
                        </div>
                    </div>
            </footer>
            <!-- End of Footer -->
        </div>
    </div>



    <script src="/bootstraplibrary/js/bootstrap.js"></script>
    <script src="/js/jquery-3.6.1.min.js"></script>
    <script src="/owlcarousel/owl.carousel.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.owl-carousel').owlCarousel({
                stagePadding: 150,
                center: false,
                loop: false,
                margin: 20,
                // nav: true,
                // navContainer: '#nav-custom',
                dots: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                }
            })
        });
    </script>

</body>

</html>
