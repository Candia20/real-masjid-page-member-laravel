<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Set a new password</title>
    <link rel="icon" href="/image/logoRm.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="/css/authentication.css">
    <link rel="stylesheet" href="/bootstraplibrary/css/bootstrap.css">
</head>

<body>

    <div class="container">
        <div class="row align-item-center mt-5 mb-5">
            <div class="col-md-5" id="card-glass-1">
                <div class="card-glass text-center d-flex align-items-center justify-content-center">
                    <img src="/image/logoRm2.png" alt="Logo Real Masjid" class="img-fluid">
                </div>
            </div>
            <div class="col-md-7">
                <div class="card-glass d-flex align-items-center" id="card-glass-2">
                    <div class="w-100">
                        <h2>Buat password baru.</h2>
                        <p class="mb-5 f-14">Password harus mengandung 8 karakter!</p>

                        <form action="/login">
                            <label for="inputPassword" class="form-label">New Password</label>
                            <input type="password" class="form-control mb-1" id="inputPassword"
                                placeholder="Isikan password baru kamu">
                            <label for="inputPasswordConfrim" class="form-label">Confirm New Password</label>
                            <input type="password" class="form-control mb-5" id="inputPasswordConfrim"
                                placeholder="Konfirmasi password baru kamu">
                            <button type="submit" id="btn-masuk" class="btn btn-large btn-masuk mb-3">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>







    <script src="/bootstraplibrary/js/bootstrap.js"></script>
</body>

</html>
