<!-- <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Email Verifikasi</title>
    <style>
        .container {
            width: 600px;
            margin-left: auto;
            margin-right: auto;
        }

        .bg-email {
            width: 100%;
            min-height: 104px;
            background: linear-gradient(360deg, #FDD99C 0%, rgba(248, 203, 148, 0.8685) 63.54%, rgba(238, 169, 100, 0.65) 100%);
            padding-left: 24px;
            padding-right: 24px;
        }

        .isi-email {
            padding: 48px 24px;
        }

        .btn-utama {
            color: white;
            background: #E9A759;
            border-radius: 5px;
            padding: 10px 20px;
            gap: 10px;
        }

        footer #bg-email {
            min-height: 291px;
        }

        footer, ul, li {
            margin-left: 12px;
            margin-right: 12px;
        }

        footer ul li i {
            color: white;
        }

        footer hr {
            color: white;
        }

        footer p {
            color: white;
        }

        footer span {
            color: white;
        }
    </style>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
    <script src="https://kit.fontawesome.com/59f485dc95.js" crossorigin="anonymous"></script>
</head>

<body>

    <header class="header-email">
        <div class="container">
            <div class="row bg-email">
                <div class="col-md-6 d-flex align-items-center justify-content-start">
                    <img src="/image/logorm-putih.png" alt="logorm-putih">
                </div>
                <div class="col-md-6 d-flex align-items-end justify-content-end">
                    <img src="/image/masjid.png" alt="ilustrasi masjid">
                </div>
            </div>
        </div>
    </header>

    <section class="isi-email">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-3">
                    <h6>Assalamualaikum, Sobat Real Masjid 😊</h6>
                    <p>Kamu telah berhasil membuat akun di website real masjid.</p>
                    <p>Tinggal satu langkah lagi untuk memverifikasi akun kamu supaya bisa digunakan, dengan klik tombol
                        <strong>verifikasi akun</strong>
                        dibawah ini ya!
                    </p>
                </div>
                <div class="col-md-12 d-flex justify-content-center mb-4">
                    <button class="btn btn-utama" type="button">Verifikasi Akun</button>
                </div>
                <div class="col-md-12">
                    <p>Terimakasih banyak sudah mendaftar akun di real masjid, salam mesra selalu.</p>
                </div>
                <hr>
                <div class="col-md-12">
                    <p>Jika kamu ada kendala tidak bisa menekan tombol verifikasi akun, lakukan copy dan paste pada URL
                        dibawah ini ke web
                        browser kamu.</p>
                    <a href="#">https://www.figma.com/file/49noSm9nsn9GEqttwN55GK/UI-Design?node-id=393%3A14025</a>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container text-center">
            <div id="bg-email" class="row bg-email">
                <div class="col-md-12">
                    <ul class="list-unstyled d-flex justify-content-center mt-5">
                        <li>
                            <a href="#"><i class="fa-brands fa-square-facebook fa-2xl"></i></a>
                        </li>
                        <li>
                            <a href=" #"><i class="fa-brands fa-instagram fa-2xl"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa-brands fa-twitter fa-2xl"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa-brands fa-tiktok fa-2xl"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa-brands fa-youtube fa-2xl"></i></a>
                        </li>
                    </ul>
                    <hr>
                    <p>Copyright &copy; Real Masjid 2.0, All rights reversed</p>
                    <p>Untuk mengetahui informasi lebih lanjut mengenai Real masjid 2.0, bisa hubungi kami di kontak
                        berkut</p>
                    <span>Email: marbot@realmasjid.com</span><br>
                    <span>Whatsapp: +62 088 2232 2321 </span>
                </div>
            </div>
        </div>
    </footer>





    <script src="/bootstraplibrary/js/bootstrap.js"></script>
</body>

</html> -->


<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Email Verifikasi</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <style>
        .container {
            width: 600px;
            margin-left: auto;
            margin-right: auto;
        }

        .bg-email {
            width: 100%;
            min-height: 104px;
            background: linear-gradient(360deg, #FDD99C 0%, rgba(248, 203, 148, 0.8685) 63.54%, rgba(238, 169, 100, 0.65) 100%);
            padding-left: 24px;
            padding-right: 24px;
        }

        .isi-email {
            padding: 48px 24px;
        }

        .btn-utama {
            color: white;
            background: #E9A759;
            border-radius: 5px;
            padding: 10px 20px;
            gap: 10px;
        }

        footer .bg-email {
            min-height: 291px;
        }

        footer ul li {
            margin-left: 12px;
            margin-right: 12px;
        }

        footer ul li i {
            color: white;
        }

        footer hr {
            color: white;
        }

        footer p {
            color: white;
        }

        footer span {
            color: white;
        }
    </style>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
    <script src="https://kit.fontawesome.com/59f485dc95.js" crossorigin="anonymous"></script>
</head>

<body>
    <header class="header-email">
        <div class="container">
            <div class="row bg-email">
                <div class="col-md-6 d-flex align-items-center justify-content-start">
                    <img src="/image/logorm-putih.png" alt="logorm-putih">
                </div>
                <div class="col-md-6 d-flex align-items-end justify-content-end">
                    <img src="/image/masjid.png" alt="ilustrasi masjid">
                </div>
            </div>
        </div>
    </header>

    <section class="isi-email">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-3">
                    <h6>Assalamualaikum, Sobat Real Masjid 😊</h6>
                    <p>Kamu telah berhasil membuat akun di website real masjid.</p>
                    <p>Tinggal satu langkah lagi untuk memverifikasi akun kamu supaya bisa digunakan, dengan klik tombol
                        <strong>verifikasi akun</strong>
                        dibawah ini ya!
                    </p>
                </div>
                <div class="col-md-12 d-flex justify-content-center mb-4">
                    <button class="btn btn-utama" type="button" onclick="window.location.href='/'">Verifikasi Akun</button>
                </div>
                <div class="col-md-12">
                    <p>Terimakasih banyak sudah mendaftar akun di real masjid, salam mesra selalu.</p>
                </div>
                <hr>
                <div class="col-md-12">
                    <p>Jika kamu ada kendala tidak bisa menekan tombol verifikasi akun, lakukan copy dan paste pada URL
                        dibawah ini ke web
                        browser kamu.</p>
                    <a href="#">https://www.figma.com/file/49noSm9nsn9GEqttwN55GK/UI-Design?node-id=393%3A14025</a>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container text-center">
            <div class="row bg-email">
                <div class="col-md-12">
                    <ul class="list-unstyled d-flex justify-content-center mt-5">
                        <li>
                            <a href="#"><i class="fa-brands fa-square-facebook fa-2xl"></i></a>
                        </li>
                        <li>
                            <a href=" #"><i class="fa-brands fa-instagram fa-2xl"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa-brands fa-twitter fa-2xl"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa-brands fa-tiktok fa-2xl"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa-brands fa-youtube fa-2xl"></i></a>
                        </li>
                    </ul>
                    <hr>
                    <p>Copyright &copy; Real Masjid 2.0, All rights reversed</p>
                    <p>Untuk mengetahui informasi lebih lanjut mengenai Real masjid 2.0, bisa hubungi kami di kontak
                        berkut</p>
                    <span>Email: marbot@realmasjid.com</span><br>
                    <span>Whatsapp: +62 088 2232 2321 </span>
                </div>
            </div>
        </div>
    </footer>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</body>

</html>
