<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Register</title>
    <link rel="icon" href="/image/logoRm.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="/css/authentication.css">
    <link rel="stylesheet" href="/bootstraplibrary/css/bootstrap.css">
</head>

<body>

    <div class="container">
        <div class="row align-item-center mt-5 mb-5">
            <div class="col-md-5" id="card-glass-1">
                <div class="card-glass text-center d-flex align-items-center justify-content-center">
                    <img src="/image/logoRm2.png" alt="Logo Real Masjid" class="img-fluid">
                </div>
            </div>
            <div class="col-md-7">
                <div class="card-glass" id="card-glass-2">
                    <h2>Daftar.</h2>
                    <p class="mb-5  f-14">Yuk jadi bagian sobat Real Masjid!</p>
                    <form action="/verify-email">
                        <label for="inputName" class="form-label">Nama Lengkap</label>
                        <input type="text" class="form-control mb-1" id="inputName"
                            placeholder="Isikan nama lengkap kamu">
                        <label for="inputNomor" class="form-label">Nomor Handphone</label>
                        <input type="text" class="form-control mb-1" id="inputNomor"
                            placeholder="Isikan nomor HP kamu">
                        <label for="datepicker" class="form-label">Tanggal Lahir</label>
                        {{-- <input type="text" class="form-control mb-1" id="datepicker"
                            placeholder=" Isikan tanggal lahir kamu"> --}}
                        <div class="input-group date" id="datepicker">
                            <input type="text" class="form-control mb-1" id="date"
                                placeholder=" Isikan tanggal lahir kamu" />
                            <span class="input-group-append">
                                <span class="input-group-text bg-light d-block">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </span>
                        </div>
                        <label for="inputEmail4" class="form-label">Email</label>
                        <input type="email" class="form-control mb-1" id="inputEmail4"
                            placeholder="Isikan alamat email kamu">
                        <label for="inputPassword4" class="form-label">Password</label>
                        <input type="password" class="form-control mb-5" id="inputPassword4"
                            placeholder="Isikan password kamu">
                        <button type="submit" id="btn-masuk" class="btn btn-large btn-masuk mb-3">Daftar</button>
                        <img src="/image/deviderOr.png" alt="devider Or" class="mb-3" width="100%">
                        <button type="submit" class="btn btn-large btn-google mb-4"><img src="/image/logoGoogle.png"
                                alt="Logo Google" width="20" height="20"> Daftar
                            dengan Google</button>
                        <p class="text-center f-14">Sudah punya akun? <a href="/login" class="link"><strong> Login
                                    disini</strong></a></p>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="/bootstraplibrary/js/bootstrap.js"></script>
    <script src="/js/jquery-3.6.1.min.js"></script>
    <script src="https://use.fontawesome.com/3fbfff7a84.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script src="/js/script.js"></script>
</body>

</html>
