<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="/image/logoRm.png">

    <!-- CSS -->
    <link rel="stylesheet" href="/css/home-kajian-list.css" />

    <!-- FONT -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@700&display=swap" rel="stylesheet" />

    <!-- ICON -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" />

    <!-- OWL CAROUSEL -->
    <link rel="stylesheet" href="/owlcarousel/assets/owl.carousel.min.css" />
    <link rel="stylesheet" href="/owlcarousel/assets/owl.theme.default.min.css" />

    <!-- RELOAD DATA DARI COMPONENTS -->
    <script src="/jquery-3.6.4/jquery.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-1.10.2.js"></script> -->

    <title>List Kajian</title>
    <link rel="stylesheet" href="/bootstraplibrary/css/bootstrap.css">
</head>

<body>

    <!-- RELOAD NAVBAR DARI COMPONENTS -->
    <div id="navbar-placeholder"></div>
    <script>
        $(function() {
            $("#navbar-placeholder").load("/components/navbar.html");
        });
    </script>

    <!-- BANNER -->
    <div class="container-banner-1 jumbotron container-fluid text-light p-5">
        <div class="item-banner-1 col-md-6">
            <h1 class="judul-item-banner-1 display-4">Fikih Masjid</h1>
            <hr />
            <h5 class="paragraf-item-banner-1">
                Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan
                bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan
                dengan menjadikan rumah Allah sebagai tempat peradaban islam.
            </h5>

            <div class="button-item-banner-1">
                <button type="button" class="button-putar-item-banner-1 btn btn-light text-dark bi bi-play-fill">
                    Putar
                </button>
                <button type="button" class="btn btn-secondary bi bi-info-circle">
                    More Info
                </button>
            </div>
        </div>
    </div>

    <!-- CONTENT -->

    <!-- Kajian Release Terbaru -->
    <section class="container-content">
        <div class="main-content">
            <h5 class="judul-content">Kajian Release Terbaru</h5>
            <div id="carousel-1" class="owl-carousel owl-theme">
                <div class="item" onclick="window.location.href='/login'">
                    <img src="/image/kajian-1.png" alt="kajian-1" />
                </div>
                <div class="item">
                    <img src="/image/kajian-2.png" alt="kajian-2" />
                </div>
                <div class="item">
                    <img src="/image/kajian-3.png" alt="kajian-3" />
                </div>
                <div class="item">
                    <img src="/image/kajian-4.png" alt="kajian-4" />
                </div>
                <div class="item">
                    <img src="/image/kajian-5.png" alt="kajian-5" />
                </div>
                <div class="item">
                    <img src="/image/kajian-6.png" alt="kajian-6" />
                </div>
                <div class="item">
                    <img src="/image/kajian-7.png" alt="kajian-7" />
                </div>
                <div class="item">
                    <img src="/image/kajian-8.png" alt="kajian-8" />
                </div>
                <div class="item">
                    <img src="/image/kajian-9.png" alt="kajian-9" />
                </div>
                <div class="item">
                    <img src="/image/kajian-10.png" alt="kajian-10" />
                </div>
            </div>
            <div class="owl-theme">
                <div class="owl-controls">
                    <div id="carousel-1-nav" class="custom-nav owl-nav"></div>
                </div>
            </div>
        </div>

        <!-- Sedang Trending -->
        <div class="main-content">
            <h5 class="judul-content">Sedang Trending</h5>
            <div id="carousel-2" class="owl-carousel owl-theme">
                <div class="item">
                    <img src="/image/kajian-1.png" alt="kajian-1" />
                </div>
                <div class="item">
                    <img src="/image/kajian-2.png" alt="kajian-2" />
                </div>
                <div class="item">
                    <img src="/image/kajian-3.png" alt="kajian-3" />
                </div>
                <div class="item">
                    <img src="/image/kajian-4.png" alt="kajian-4" />
                </div>
                <div class="item">
                    <img src="/image/kajian-5.png" alt="kajian-5" />
                </div>
                <div class="item">
                    <img src="/image/kajian-6.png" alt="kajian-6" />
                </div>
                <div class="item">
                    <img src="/image/kajian-7.png" alt="kajian-7" />
                </div>
                <div class="item">
                    <img src="/image/kajian-8.png" alt="kajian-8" />
                </div>
                <div class="item">
                    <img src="/image/kajian-9.png" alt="kajian-9" />
                </div>
                <div class="item">
                    <img src="/image/kajian-10.png" alt="kajian-10" />
                </div>
            </div>
            <div class="owl-theme">
                <div class="owl-controls">
                    <div id="carousel-2-nav" class="custom-nav owl-nav"></div>
                </div>
            </div>
        </div>

        <!-- Kajian Fikih -->
        <div class="main-content">
            <h5 class="judul-content">Kajian Fikih</h5>
            <div id="carousel-3" class="owl-carousel owl-theme">
                <div class="item">
                    <img src="/image/kajian-1.png" alt="kajian-1" />
                </div>
                <div class="item">
                    <img src="/image/kajian-2.png" alt="kajian-2" />
                </div>
                <div class="item">
                    <img src="/image/kajian-3.png" alt="kajian-3" />
                </div>
                <div class="item">
                    <img src="/image/kajian-4.png" alt="kajian-4" />
                </div>
                <div class="item">
                    <img src="/image/kajian-5.png" alt="kajian-5" />
                </div>
                <div class="item">
                    <img src="/image/kajian-6.png" alt="kajian-6" />
                </div>
                <div class="item">
                    <img src="/image/kajian-7.png" alt="kajian-7" />
                </div>
                <div class="item">
                    <img src="/image/kajian-8.png" alt="kajian-8" />
                </div>
                <div class="item">
                    <img src="/image/kajian-9.png" alt="kajian-9" />
                </div>
                <div class="item">
                    <img src="/image/kajian-10.png" alt="kajian-10" />
                </div>
            </div>
            <div class="owl-theme">
                <div class="owl-controls">
                    <div id="carousel-3-nav" class="custom-nav owl-nav"></div>
                </div>
            </div>
        </div>

        <!-- Starday Nite -->
        <div class="main-content">
            <h5 class="judul-content">Starday Nite</h5>
            <div id="carousel-4" class="owl-carousel owl-theme">
                <div class="item">
                    <img src="/image/kajian-1.png" alt="kajian-1" />
                </div>
                <div class="item">
                    <img src="/image/kajian-2.png" alt="kajian-2" />
                </div>
                <div class="item">
                    <img src="/image/kajian-3.png" alt="kajian-3" />
                </div>
                <div class="item">
                    <img src="/image/kajian-4.png" alt="kajian-4" />
                </div>
                <div class="item">
                    <img src="/image/kajian-5.png" alt="kajian-5" />
                </div>
                <div class="item">
                    <img src="/image/kajian-6.png" alt="kajian-6" />
                </div>
                <div class="item">
                    <img src="/image/kajian-7.png" alt="kajian-7" />
                </div>
                <div class="item">
                    <img src="/image/kajian-8.png" alt="kajian-8" />
                </div>
                <div class="item">
                    <img src="/image/kajian-9.png" alt="kajian-9" />
                </div>
                <div class="item">
                    <img src="/image/kajian-10.png" alt="kajian-10" />
                </div>
            </div>
            <div class="owl-theme">
                <div class="owl-controls">
                    <div id="carousel-4-nav" class="custom-nav owl-nav"></div>
                </div>
            </div>
        </div>

        <!-- Kelas Kisah Nabi <-->
        <div class="main-content">
            <h5 class="judul-content">Kelas Kisah Nabi</h5>
            <div id="carousel-5" class="owl-carousel owl-theme">
                <div class="item">
                    <img src="/image/kajian-1.png" alt="kajian-1" />
                </div>
                <div class="item">
                    <img src="/image/kajian-2.png" alt="kajian-2" />
                </div>
                <div class="item">
                    <img src="/image/kajian-3.png" alt="kajian-3" />
                </div>
                <div class="item">
                    <img src="/image/kajian-4.png" alt="kajian-4" />
                </div>
                <div class="item">
                    <img src="/image/kajian-5.png" alt="kajian-5" />
                </div>
                <div class="item">
                    <img src="/image/kajian-6.png" alt="kajian-6" />
                </div>
                <div class="item">
                    <img src="/image/kajian-7.png" alt="kajian-7" />
                </div>
                <div class="item">
                    <img src="/image/kajian-8.png" alt="kajian-8" />
                </div>
                <div class="item">
                    <img src="/image/kajian-9.png" alt="kajian-9" />
                </div>
                <div class="item">
                    <img src="/image/kajian-10.png" alt="kajian-10" />
                </div>
            </div>
            <div class="owl-theme">
                <div class="owl-controls">
                    <div id="carousel-5-nav" class="custom-nav owl-nav"></div>
                </div>
            </div>
        </div>

        <!-- Kajian Tafsir -->
        <div class="main-content">
            <h5 class="judul-content">Kajian Tafsir</h5>
            <div id="carousel-6" class="owl-carousel owl-theme">
                <div class="item">
                    <img src="/image/kajian-1.png" alt="kajian-1" />
                </div>
                <div class="item">
                    <img src="/image/kajian-2.png" alt="kajian-2" />
                </div>
                <div class="item">
                    <img src="/image/kajian-3.png" alt="kajian-3" />
                </div>
                <div class="item">
                    <img src="/image/kajian-4.png" alt="kajian-4" />
                </div>
                <div class="item">
                    <img src="/image/kajian-5.png" alt="kajian-5" />
                </div>
                <div class="item">
                    <img src="/image/kajian-6.png" alt="kajian-6" />
                </div>
                <div class="item">
                    <img src="/image/kajian-7.png" alt="kajian-7" />
                </div>
                <div class="item">
                    <img src="/image/kajian-8.png" alt="kajian-8" />
                </div>
                <div class="item">
                    <img src="/image/kajian-9.png" alt="kajian-9" />
                </div>
                <div class="item">
                    <img src="/image/kajian-10.png" alt="kajian-10" />
                </div>
            </div>
            <div class="owl-theme">
                <div class="owl-controls">
                    <div id="carousel-6-nav" class="custom-nav owl-nav"></div>
                </div>
            </div>
        </div>

        <!-- Kajian Subuh -->
        <div class="main-content">
            <h5 class="judul-content">Kajian Subuh</h5>
            <div id="carousel-7" class="owl-carousel owl-theme">
                <div class="item" data-bs-toggle="modal" data-bs-target="#myModal">
                    <div class="col zoom-in">
                        <img src="/image/kajian-1.png" alt="kajian-1" />
                    </div>
                </div>
                <div class="item">
                    <div class="col zoom-in">
                        <img src="/image/kajian-2.png" alt="kajian-2" />
                    </div>
                </div>
                <div class="item">
                    <div class="col zoom-in">
                        <img src="/image/kajian-3.png" alt="kajian-2" />
                    </div>
                </div>
                <div class="item">
                    <div class="col zoom-in">
                        <img src="/image/kajian-4.png" alt="kajian-2" />
                    </div>
                </div>
                <div class="item">
                    <div class="col zoom-in">
                        <img src="/image/kajian-5.png" alt="kajian-2" />
                    </div>
                </div>
                <div class="item">
                    <div class="col zoom-in">
                        <img src="/image/kajian-6.png" alt="kajian-2" />
                    </div>
                </div>
                <div class="item">
                    <div class="col zoom-in">
                        <img src="/image/kajian-7.png" alt="kajian-2" />
                    </div>
                </div>
                <div class="item">
                    <div class="col zoom-in">
                        <img src="/image/kajian-8.png" alt="kajian-2" />
                    </div>
                </div>
                <div class="item">
                    <div class="col zoom-in">
                        <img src="/image/kajian-9.png" alt="kajian-2" />
                    </div>
                </div>
                <div class="item">
                    <div class="col zoom-in">
                        <img src="/image/kajian-10.png" alt="kajian-2" />
                    </div>
                </div>
            </div>
            <div class="owl-theme">
                <div class="owl-controls">
                    <div id="carousel-7-nav" class="custom-nav owl-nav"></div>
                </div>
            </div>
        </div>

        <!-- Kajian Petang -->
        <div class="main-content">
            <h5 class="judul-content">Kajian Petang</h5>
            <div id="carousel-8" class="owl-carousel owl-theme">
                <div class="item">
                    <img src="/image/kajian-1.png" alt="kajian-1" />
                </div>
                <div class="item">
                    <img src="/image/kajian-2.png" alt="kajian-2" />
                </div>
                <div class="item">
                    <img src="/image/kajian-3.png" alt="kajian-3" />
                </div>
                <div class="item">
                    <img src="/image/kajian-4.png" alt="kajian-4" />
                </div>
                <div class="item">
                    <img src="/image/kajian-5.png" alt="kajian-5" />
                </div>
                <div class="item">
                    <img src="/image/kajian-6.png" alt="kajian-6" />
                </div>
                <div class="item">
                    <img src="/image/kajian-7.png" alt="kajian-7" />
                </div>
                <div class="item">
                    <img src="/image/kajian-8.png" alt="kajian-8" />
                </div>
                <div class="item">
                    <img src="/image/kajian-9.png" alt="kajian-9" />
                </div>
                <div class="item">
                    <img src="/image/kajian-10.png" alt="kajian-10" />
                </div>
            </div>
            <div class="owl-theme">
                <div class="owl-controls">
                    <div id="carousel-8-nav" class="custom-nav owl-nav"></div>
                </div>
            </div>
        </div>
        <!-- World Quran Fest -->
        <div class="main-content">
            <h5 class="judul-content">World Quran Fest</h5>
            <div id="carousel-9" class="owl-carousel owl-theme">
                <div class="item">
                    <img src="/image/kajian-1.png" alt="kajian-1" />
                </div>
                <div class="item">
                    <img src="/image/kajian-2.png" alt="kajian-2" />
                </div>
                <div class="item">
                    <img src="/image/kajian-3.png" alt="kajian-3" />
                </div>
                <div class="item">
                    <img src="/image/kajian-4.png" alt="kajian-4" />
                </div>
                <div class="item">
                    <img src="/image/kajian-5.png" alt="kajian-5" />
                </div>
                <div class="item">
                    <img src="/image/kajian-6.png" alt="kajian-6" />
                </div>
                <div class="item">
                    <img src="/image/kajian-7.png" alt="kajian-7" />
                </div>
                <div class="item">
                    <img src="/image/kajian-8.png" alt="kajian-8" />
                </div>
                <div class="item">
                    <img src="/image/kajian-9.png" alt="kajian-9" />
                </div>
                <div class="item">
                    <img src="/image/kajian-10.png" alt="kajian-10" />
                </div>
            </div>
            <div class="owl-theme">
                <div class="owl-controls">
                    <div id="carousel-9-nav" class="custom-nav owl-nav"></div>
                </div>
            </div>
        </div>
        <!-- Real Holiday -->
        <div class="main-content">
            <h5 class="judul-content">Real Holiday</h5>
            <div id="carousel-10" class="owl-carousel owl-theme">
                <div class="item">
                    <img src="/image/kajian-1.png" alt="kajian-1" />
                </div>
                <div class="item">
                    <img src="/image/kajian-2.png" alt="kajian-2" />
                </div>
                <div class="item">
                    <img src="/image/kajian-3.png" alt="kajian-3" />
                </div>
                <div class="item">
                    <img src="/image/kajian-4.png" alt="kajian-4" />
                </div>
                <div class="item">
                    <img src="/image/kajian-5.png" alt="kajian-5" />
                </div>
                <div class="item">
                    <img src="/image/kajian-6.png" alt="kajian-6" />
                </div>
                <div class="item">
                    <img src="/image/kajian-7.png" alt="kajian-7" />
                </div>
                <div class="item">
                    <img src="/image/kajian-8.png" alt="kajian-8" />
                </div>
                <div class="item">
                    <img src="/image/kajian-9.png" alt="kajian-9" />
                </div>
                <div class="item">
                    <img src="/image/kajian-10.png" alt="kajian-10" />
                </div>
            </div>
            <div class="owl-theme">
                <div class="owl-controls">
                    <div id="carousel-10-nav" class="custom-nav owl-nav"></div>
                </div>
            </div>
        </div>
        <!-- Marbot Show -->
        <div class="main-content">
            <h5 class="judul-content">Marbot Show</h5>
            <div id="carousel-11" class="owl-carousel owl-theme">
                <div class="item">
                    <img src="/image/kajian-1.png" alt="kajian-1" />
                </div>
                <div class="item">
                    <img src="/image/kajian-2.png" alt="kajian-2" />
                </div>
                <div class="item">
                    <img src="/image/kajian-3.png" alt="kajian-3" />
                </div>
                <div class="item">
                    <img src="/image/kajian-4.png" alt="kajian-4" />
                </div>
                <div class="item">
                    <img src="/image/kajian-5.png" alt="kajian-5" />
                </div>
                <div class="item">
                    <img src="/image/kajian-6.png" alt="kajian-6" />
                </div>
                <div class="item">
                    <img src="/image/kajian-7.png" alt="kajian-7" />
                </div>
                <div class="item">
                    <img src="/image/kajian-8.png" alt="kajian-8" />
                </div>
                <div class="item">
                    <img src="/image/kajian-9.png" alt="kajian-9" />
                </div>
                <div class="item">
                    <img src="/image/kajian-10.png" alt="kajian-10" />
                </div>
            </div>
            <div class="owl-theme">
                <div class="owl-controls">
                    <div id="carousel-11-nav" class="custom-nav owl-nav"></div>
                </div>
            </div>
        </div>
        <!-- Story of Taqwa -->
        <div class="main-content">
            <h5 class="judul-content">Story of Taqwa</h5>
            <div id="carousel-12" class="owl-carousel owl-theme">
                <div class="item">
                    <img src="/image/kajian-1.png" alt="kajian-1" />
                </div>
                <div class="item">
                    <img src="/image/kajian-2.png" alt="kajian-2" />
                </div>
                <div class="item">
                    <img src="/image/kajian-3.png" alt="kajian-3" />
                </div>
                <div class="item">
                    <img src="/image/kajian-4.png" alt="kajian-4" />
                </div>
                <div class="item">
                    <img src="/image/kajian-5.png" alt="kajian-5" />
                </div>
                <div class="item">
                    <img src="/image/kajian-6.png" alt="kajian-6" />
                </div>
                <div class="item">
                    <img src="/image/kajian-7.png" alt="kajian-7" />
                </div>
                <div class="item">
                    <img src="/image/kajian-8.png" alt="kajian-8" />
                </div>
                <div class="item">
                    <img src="/image/kajian-9.png" alt="kajian-9" />
                </div>
                <div class="item">
                    <img src="/image/kajian-10.png" alt="kajian-10" />
                </div>
            </div>
            <div class="owl-theme">
                <div class="owl-controls">
                    <div id="carousel-12-nav" class="custom-nav owl-nav"></div>
                </div>
            </div>
        </div>
    </section>

    <!-- MODAL -->
    <div class="modal" id="myModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal banner -->
                <div class="banner-modal-2 card">
                    <img src="/image/tumbnail-modal-1.png" class="img-banner-modal-2" alt="..." />
                    <div class="card-img-overlay">
                        <img src="/icon/gg_close-o.png" type="button" class="img-button-close-banner-modal-2" data-bs-dismiss="modal" alt="button" />
                        <div class="group-button-banner-modal-2 position-absolute bottom-0 start-0">
                            <img src="/icon/button-putar.png" type="button" alt="button" />
                            <img src="/icon/bi_plus-circle.png" type="button" class="img-button-plus-banner-modal-2" alt="button" />
                            <img src="/icon/bi_hand-thumbs-up-fill.png" type="button" alt="button" />
                        </div>
                    </div>
                </div>

                <!-- Modal body -->
                <div class="container-modal-body">
                    <div class="row justify-content-evenly mb-4">
                        <div class="col-md-7">
                            <h5>
                                2022
                                <span class="mx-3">2 Session</span>
                                <span class="border border-1 border-dark px-1">Ultra HD</span>
                            </h5>
                            <p style="color: #404040">
                                This is a wider card with supporting text below as a natural
                                lead-in to additional content. This content is a little bit
                                longer.
                            </p>
                        </div>
                        <div class="col-md-5 lh-1">
                            <p>
                                <span style="color: #7b7b7b">Ustadz :</span>
                                <span>Pago</span>
                            </p>
                            <p>
                                <span style="color: #7b7b7b">Tanggal :</span>
                                <span>12 Juni s/d 12 Desember 2022</span>
                            </p>
                            <p>
                                <span style="color: #7b7b7b">Tempat :</span>
                                <span>Real Masjid 2.0</span>
                            </p>
                            <p>
                                <span style="color: #7b7b7b">Jam :</span>
                                <span>19:30 WIB</span>
                            </p>
                        </div>
                    </div>

                    <!-- Button episode -->
                    <div class="row justify-content-between mb-4">
                        <div class="col-4">
                            <h3>Episode</h3>
                        </div>
                        <div class="col-4 text-end">
                            <div class="btn-group" id="myBtnContainer">
                                <button type="button" class="dropdown-toggle border-1 rounded-0 bg-white fs-5" data-bs-toggle="dropdown" aria-expanded="false">
                                    Session
                                </button>
                                <ul class="dropdown-menu dropdown-menu-end">
                                    <!-- <li>
                      <button
                        class="dropdown-item btn active"
                        onclick="filterSelection('all')"
                      >
                        Show all
                      </button>
                    </li> -->
                                    <li>
                                        <button class="dropdown-item" type="button" onclick="filterSelection('seasion-1')">
                                            Session 1
                                        </button>
                                    </li>
                                    <li>
                                        <button class="dropdown-item" type="button" onclick="filterSelection('seasion-2')">
                                            Session 2
                                        </button>
                                    </li>
                                    <li>
                                        <button class="dropdown-item" type="button" onclick="filterSelection('seasion-3')">
                                            Session 3
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- Card episode -->
                    <div class="">
                        <div class="filterDiv seasion-1 card  p-1 rounded-0 border border-bottom-0 border-end-0 border-start-0">
                            <div class="row g-0">
                                <div class="col-md-1 d-flex justify-content-center align-items-center">
                                    <h4>1</h4>
                                </div>
                                <div class="col-md-3 d-flex justify-content-center align-items-center">
                                    <img src="/image/kisah-nabi-adam.png" class="img-fluid rounded-start" alt="..." />
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <h5 class="card-title">Kisah Nabi Adam</h5>
                                        <p class="card-text justify-content-center">
                                            This is a wider card with supporting text below as a
                                            natural lead-in to additional content. This content is a
                                            little bit longer.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="filterDiv seasion-1 card p-1 rounded-0 border border-bottom-0 border-end-0 border-start-0">
                            <div class="row g-0">
                                <div class="col-md-1 d-flex justify-content-center align-items-center">
                                    <h4>2</h4>
                                </div>
                                <div class="col-md-3 d-flex justify-content-center align-items-center">
                                    <img src="/image/kisah-nabi-adam.png" class="img-fluid rounded-start" alt="..." />
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <h5 class="card-title">Kisah Nabi Adam</h5>
                                        <p class="card-text">
                                            This is a wider card with supporting text below as a
                                            natural lead-in to additional content. This content is a
                                            little bit longer.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="filterDiv seasion-2 card p-1 rounded-0 border border-bottom-0 border-end-0 border-start-0">
                            <div class="row g-0">
                                <div class="col-md-1 d-flex justify-content-center align-items-center">
                                    <h4>1</h4>
                                </div>
                                <div class="col-md-3 d-flex justify-content-center align-items-center">
                                    <img src="/image/kisah-nabi-isa.png" class="img-fluid rounded-start" alt="..." />
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <h5 class="card-title">Kisah Nabi Isa</h5>
                                        <p class="card-text">
                                            This is a wider card with supporting text below as a
                                            natural lead-in to additional content. This content is a
                                            little bit longer.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="filterDiv seasion-3 card p-1 rounded-0 border border-end-0 border-start-0">
                            <div class="row g-0">
                                <div class="col-md-1 d-flex justify-content-center align-items-center">
                                    <h4>1</h4>
                                </div>
                                <div class="col-md-3 d-flex justify-content-center align-items-center">
                                    <img src="/image/kisah-nabi-nuh.png" class="img-fluid rounded-start" alt="..." />
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <h5 class="card-title">Kisah Nabi Nuh<span class="justify-content-end align-items-end">52m</span></h5>
                                        <p class="card-text">
                                            This is a wider card with supporting text below as a
                                            natural lead-in to additional content. This content is a
                                            little bit longer.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div>
                        <h3 onclick="myFunction()" class="text-center">
                            <i class="fa-solid fa-circle-chevron-down"></i>
                        </h3>
                    </div>
                    <div id="myDIV">
                        <div class="row row-cols-1 row-cols-md-3 g-4">
                            <div class="col">
                                <div class="card">
                                    <img src="/image/kajian-4.png" class="card-img-top" alt="..." />
                                    <div class="card-body">
                                        <h6 class="card-title fw-bold">
                                            Trailer Kelas Kisah Nabi - Seasion 1
                                        </h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card">
                                    <img src="/image/kajian-3.png" class="card-img-top" alt="..." />
                                    <div class="card-body">
                                        <h6 class="card-title fw-bold">
                                            Trailer Kelas Kisah Nabi - Seasion 1
                                        </h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card">
                                    <img src="/image/kajian-4.png" class="card-img-top" alt="..." />
                                    <div class="card-body">
                                        <h6 class="card-title fw-bold">
                                            Trailer Kelas Kisah Nabi - Seasion 1
                                        </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Control-->
    <script>
        function myFunction() {
            var x = document.getElementById("myDIV");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }
    </script>

    <!-- RELOAD FOOTER DARI COMPONENTS -->
    <div id="footer-placeholder"></div>
    <script>
        $(function() {
            $("#footer-placeholder").load("/components/footer.html");
        });
    </script>

    <!-- BOOTSTRAP CDN -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"></script>

    <!-- JQUERY CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <!-- CAROUSEL -->
    <!-- Carousel CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <!-- Carousel Control  -->
    <script>
        $("#carousel-1").owlCarousel({
            stagePadding: 50,
            loop: true,
            margin: 10,
            dots: false,
            nav: true,
            navText: [
                '<i class="fa-solid fa-chevron-left" aria-hidden="true"></i>',
                '<i class="fa-solid fa-chevron-right" aria-hidden="true"></i>',
            ],
            navContainer: "#carousel-1-nav",
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 3,
                },
                1000: {
                    items: 5,
                },
            },
        });

        $("#carousel-2").owlCarousel({
            stagePadding: 50,
            loop: true,
            margin: 10,
            dots: false,
            nav: true,
            navText: [
                '<i class="fa-solid fa-chevron-left" aria-hidden="true"></i>',
                '<i class="fa-solid fa-chevron-right" aria-hidden="true"></i>',
            ],
            navContainer: "#carousel-2-nav",
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 3,
                },
                1000: {
                    items: 5,
                },
            },
        });

        $("#carousel-3").owlCarousel({
            stagePadding: 50,
            loop: true,
            margin: 10,
            dots: false,
            nav: true,
            navText: [
                '<i class="fa-solid fa-chevron-left" aria-hidden="true"></i>',
                '<i class="fa-solid fa-chevron-right" aria-hidden="true"></i>',
            ],
            navContainer: "#carousel-3-nav",
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 3,
                },
                1000: {
                    items: 5,
                },
            },
        });

        $("#carousel-4").owlCarousel({
            stagePadding: 50,
            loop: true,
            margin: 10,
            dots: false,
            nav: true,
            navText: [
                '<i class="fa-solid fa-chevron-left" aria-hidden="true"></i>',
                '<i class="fa-solid fa-chevron-right" aria-hidden="true"></i>',
            ],
            navContainer: "#carousel-4-nav",
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 3,
                },
                1000: {
                    items: 5,
                },
            },
        });

        $("#carousel-5").owlCarousel({
            stagePadding: 50,
            loop: true,
            margin: 10,
            dots: false,
            nav: true,
            navText: [
                '<i class="fa-solid fa-chevron-left" aria-hidden="true"></i>',
                '<i class="fa-solid fa-chevron-right" aria-hidden="true"></i>',
            ],
            navContainer: "#carousel-5-nav",
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 3,
                },
                1000: {
                    items: 5,
                },
            },
        });

        $("#carousel-6").owlCarousel({
            stagePadding: 50,
            loop: true,
            margin: 10,
            dots: false,
            nav: true,
            navText: [
                '<i class="fa-solid fa-chevron-left" aria-hidden="true"></i>',
                '<i class="fa-solid fa-chevron-right" aria-hidden="true"></i>',
            ],
            navContainer: "#carousel-6-nav",
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 3,
                },
                1000: {
                    items: 5,
                },
            },
        });

        $("#carousel-7").owlCarousel({
            stagePadding: 50,
            loop: true,
            margin: 10,
            dots: false,
            nav: true,
            navText: [
                '<i class="fa-solid fa-chevron-left" aria-hidden="true"></i>',
                '<i class="fa-solid fa-chevron-right" aria-hidden="true"></i>',
            ],
            navContainer: "#carousel-7-nav",
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 3,
                },
                1000: {
                    items: 5,
                },
            },
        });

        $("#carousel-8").owlCarousel({
            stagePadding: 50,
            loop: true,
            margin: 10,
            dots: false,
            nav: true,
            navText: [
                '<i class="fa-solid fa-chevron-left" aria-hidden="true"></i>',
                '<i class="fa-solid fa-chevron-right" aria-hidden="true"></i>',
            ],
            navContainer: "#carousel-8-nav",
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 3,
                },
                1000: {
                    items: 5,
                },
            },
        });

        $("#carousel-9").owlCarousel({
            stagePadding: 50,
            loop: true,
            margin: 10,
            dots: false,
            nav: true,
            navText: [
                '<i class="fa-solid fa-chevron-left" aria-hidden="true"></i>',
                '<i class="fa-solid fa-chevron-right" aria-hidden="true"></i>',
            ],
            navContainer: "#carousel-9-nav",
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 3,
                },
                1000: {
                    items: 5,
                },
            },
        });

        $("#carousel-10").owlCarousel({
            stagePadding: 50,
            loop: true,
            margin: 10,
            dots: false,
            nav: true,
            navText: [
                '<i class="fa-solid fa-chevron-left" aria-hidden="true"></i>',
                '<i class="fa-solid fa-chevron-right" aria-hidden="true"></i>',
            ],
            navContainer: "#carousel-10-nav",
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 3,
                },
                1000: {
                    items: 5,
                },
            },
        });

        $("#carousel-11").owlCarousel({
            stagePadding: 50,
            loop: true,
            margin: 10,
            dots: false,
            nav: true,
            navText: [
                '<i class="fa-solid fa-chevron-left" aria-hidden="true"></i>',
                '<i class="fa-solid fa-chevron-right" aria-hidden="true"></i>',
            ],
            navContainer: "#carousel-11-nav",
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 3,
                },
                1000: {
                    items: 5,
                },
            },
        });

        $("#carousel-12").owlCarousel({
            stagePadding: 50,
            loop: true,
            margin: 10,
            dots: false,
            nav: true,
            navText: [
                '<i class="fa-solid fa-chevron-left" aria-hidden="true"></i>',
                '<i class="fa-solid fa-chevron-right" aria-hidden="true"></i>',
            ],
            navContainer: "#carousel-12-nav",
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 3,
                },
                1000: {
                    items: 5,
                },
            },
        });
    </script>







    <script src="/bootstraplibrary/js/bootstrap.js"></script>
</body>

</html>
