<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- FONT -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet" />

    <!-- ICON -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" />
    <title>Detail Kajian</title>

    <!-- CSS -->
    <link rel="stylesheet" href="/css/home-Kajian-detail.css" />
    <link rel="stylesheet" href="/bootstraplibrary/css/bootstrap.css">
</head>

<body>

    <!-- MODAL -->
    <div class="modal-content">
        <!-- Modal banner -->
        <div class="banner-modal-2 card">
            <img src="/image/tumbnail-modal-1.png" class="..." alt="image" />
            <div class="card-img-overlay mb-5 ms-5">
                <div class="position-absolute bottom-0 start-0">
                    <img src="/icon/button-putar.png" type="button" alt="button" />
                    <img src="/icon/bi_plus-circle.png" type="button" class="img-button-plus-banner-modal-2" alt="button" />
                    <img src="/icon/bi_hand-thumbs-up-fill.png" type="button" alt="button" />
                </div>
            </div>
        </div>

        <!-- Modal body -->
        <div class="container-modal-body">
            <div class="row justify-content-evenly mb-4">
                <div class="col-md-8">
                    <h5 class="fw-semibold">
                        2022
                        <span class="mx-3">2 season</span>
                        <span class="border border-1 border-dark px-1">Ultra HD</span>
                    </h5>
                    <p style="color: #404040">
                        Kelas yang sangat menarik untuk mengenal para Nabi yang telah di
                        utus Allah ke dunia untuk meluruskan manusia, dengan cerita yang
                        sangat menarik dan membuat kagum.
                    </p>
                </div>
                <div class="col-md-4 lh-1">
                    <p>
                        <span style="color: #7b7b7b">Ustadz :</span>
                        <span>Pago</span>
                    </p>
                    <p class="lh-base">
                        <span style="color: #7b7b7b">Tanggal :</span>
                        <span>12 Juni s/d 12 Desember 2022</span>
                    </p>
                    <p>
                        <span style="color: #7b7b7b">Tempat :</span>
                        <span>Real Masjid 2.0</span>
                    </p>
                    <p>
                        <span style="color: #7b7b7b">Jam :</span>
                        <span>19:30 WIB</span>
                    </p>
                </div>
            </div>

            <!-- Button episode -->
            <div class="row mb-4">
                <div class="col-6 text-start">
                    <h4 class="fw-bold">Episode</h4>
                </div>
                <div class="col-6 text-end">
                    <div class="btn-group" id="myBtnContainer">
                        <button type="button" class="dropdown-toggle border-1 rounded-0 bg-white fs-5 fw-bold" data-bs-toggle="dropdown" aria-expanded="false">
                            Season
                        </button>
                        <ul class="dropdown-menu dropdown-menu-end">
                            <li>
                                <button class="dropdown-item" type="button" onclick="filterSelection('season-1')">
                                    Season 1
                                </button>
                            </li>
                            <li>
                                <button class="dropdown-item" type="button" onclick="filterSelection('season-2')">
                                    Season 2
                                </button>
                            </li>
                            <li>
                                <button class="dropdown-item" type="button" onclick="filterSelection('season-3')">
                                    Season 3
                                </button>
                            </li>
                            <li>
                                <button class="dropdown-item" type="button" onclick="filterSelection('season-4')">
                                    Season 4
                                </button>
                            </li>
                            <div class="container border border-bottom-0 border-end-0 border-start-0 my-2"></div>
                            <li>
                                <button class="dropdown-item" onclick="filterSelection('all')">
                                    Lihat Semua Episode
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Card episode -->
            <div class="border border-2 border-top-0 border-end-0 border-start-0 pb-3">
                <div class="filterDiv season-1 p-1 rounded-0 border border-1 border-bottom-0 border-end-0 border-start-0">
                    <div class="sub-item-episode row g-0">
                        <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <h4 class="fw-semibold">1</h4>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center align-items-center mb-3 mb-md-0">
                            <img src="/image/kisah-nabi-adam.png" class="img-fluid rounded-start w-75" alt="..." />
                        </div>
                        <div class="col-md-8 d-flex justify-content-center align-items-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col col-9">
                                        <h5 class="fw-semibold">Kisah Nabi Adam</h5>
                                    </div>
                                    <div class="col col-3 text-end">
                                        <h5 class="fw-semibold">52m</h5>
                                    </div>
                                </div>
                                <p class="card-text">
                                    Mempelajadi berbagai macam hal yang berkaitan dengan masjid
                                    dan bagaimana cara memakmurkannya, banyak pelajaran mendasar
                                    berkaitan dengan menjadikan rumah Allah sebagai tempat
                                    peradaban islam.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="filterDiv season-1 p-1 rounded-0 border border-1 border-bottom-0 border-end-0 border-start-0">
                    <div class="sub-item-episode row g-0">
                        <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <h4 class="fw-semibold">2</h4>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center align-items-center mb-3 mb-md-0">
                            <img src="/image/kisah-nabi-adam.png" class="img-fluid rounded-start w-75" alt="..." />
                        </div>
                        <div class="col-md-8 d-flex justify-content-center align-items-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col col-9">
                                        <h5 class="fw-semibold">Kisah Nabi Yusuf</h5>
                                    </div>
                                    <div class="col col-3 text-end">
                                        <h5 class="fw-semibold">52m</h5>
                                    </div>
                                </div>
                                <p class="card-text justify-content-center">
                                    Mempelajadi berbagai macam hal yang berkaitan dengan masjid
                                    dan bagaimana cara memakmurkannya, banyak pelajaran mendasar
                                    berkaitan dengan menjadikan rumah Allah sebagai tempat
                                    peradaban islam.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="filterDiv season-1 p-1 rounded-0 border border-1 border-bottom-0 border-end-0 border-start-0">
                    <div class="sub-item-episode row g-0">
                        <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <h4 class="fw-semibold">3</h4>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center align-items-center mb-3 mb-md-0">
                            <img src="/image/kisah-nabi-adam.png" class="img-fluid rounded-start w-75" alt="..." />
                        </div>
                        <div class="col-md-8 d-flex justify-content-center align-items-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col col-9">
                                        <h5 class="fw-semibold">Kisah Nabi Nuh</h5>
                                    </div>
                                    <div class="col col-3 text-end">
                                        <h5 class="fw-semibold">52m</h5>
                                    </div>
                                </div>
                                <p class="card-text justify-content-center">
                                    Mempelajadi berbagai macam hal yang berkaitan dengan masjid
                                    dan bagaimana cara memakmurkannya, banyak pelajaran mendasar
                                    berkaitan dengan menjadikan rumah Allah sebagai tempat
                                    peradaban islam.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="filterDiv season-1 p-1 rounded-0 border border-1 border-bottom-0 border-end-0 border-start-0">
                    <div class="sub-item-episode row g-0">
                        <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <h4 class="fw-semibold">4</h4>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center align-items-center mb-3 mb-md-0">
                            <img src="/image/kisah-nabi-adam.png" class="img-fluid rounded-start w-75" alt="..." />
                        </div>
                        <div class="col-md-8 d-flex justify-content-center align-items-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col col-9">
                                        <h5 class="fw-semibold">Kisah Nabi Isa</h5>
                                    </div>
                                    <div class="col col-3 text-end">
                                        <h5 class="fw-semibold">52m</h5>
                                    </div>
                                </div>
                                <p class="card-text justify-content-center">
                                    Mempelajadi berbagai macam hal yang berkaitan dengan masjid
                                    dan bagaimana cara memakmurkannya, banyak pelajaran mendasar
                                    berkaitan dengan menjadikan rumah Allah sebagai tempat
                                    peradaban islam.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="filterDiv season-1 p-1 rounded-0 border border-1 border-bottom-0 border-end-0 border-start-0">
                    <div class="sub-item-episode row g-0">
                        <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <h4 class="fw-semibold">5</h4>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center align-items-center mb-3 mb-md-0">
                            <img src="/image/kisah-nabi-adam.png" class="img-fluid rounded-start w-75" alt="..." />
                        </div>
                        <div class="col-md-8 d-flex justify-content-center align-items-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col col-9">
                                        <h5 class="fw-semibold">Kisah Nabi Hud</h5>
                                    </div>
                                    <div class="col col-3 text-end">
                                        <h5 class="fw-semibold">52m</h5>
                                    </div>
                                </div>
                                <p class="card-text justify-content-center">
                                    Mempelajadi berbagai macam hal yang berkaitan dengan masjid
                                    dan bagaimana cara memakmurkannya, banyak pelajaran mendasar
                                    berkaitan dengan menjadikan rumah Allah sebagai tempat
                                    peradaban islam.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="filterDiv season-1 p-1 rounded-0 border border-1 border-bottom-0 border-end-0 border-start-0">
                    <div class="sub-item-episode row g-0">
                        <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <h4 class="fw-semibold">6</h4>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center align-items-center mb-3 mb-md-0">
                            <img src="/image/kisah-nabi-adam.png" class="img-fluid rounded-start w-75" alt="..." />
                        </div>
                        <div class="col-md-8 d-flex justify-content-center align-items-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col col-9">
                                        <h5 class="fw-semibold">Kisah Nabi Shaleh</h5>
                                    </div>
                                    <div class="col col-3 text-end">
                                        <h5 class="fw-semibold">52m</h5>
                                    </div>
                                </div>
                                <p class="card-text justify-content-center">
                                    Mempelajadi berbagai macam hal yang berkaitan dengan masjid
                                    dan bagaimana cara memakmurkannya, banyak pelajaran mendasar
                                    berkaitan dengan menjadikan rumah Allah sebagai tempat
                                    peradaban islam.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="filterDiv season-1 p-1 rounded-0 border border-1 border-bottom-0 border-end-0 border-start-0">
                    <div class="sub-item-episode row g-0">
                        <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <h4 class="fw-semibold">7</h4>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center align-items-center mb-3 mb-md-0">
                            <img src="/image/kisah-nabi-adam.png" class="img-fluid rounded-start w-75" alt="..." />
                        </div>
                        <div class="col-md-8 d-flex justify-content-center align-items-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col col-9">
                                        <h5 class="fw-semibold">Kisah Nabi Ibrahim</h5>
                                    </div>
                                    <div class="col col-3 text-end">
                                        <h5 class="fw-semibold">52m</h5>
                                    </div>
                                </div>
                                <p class="card-text justify-content-center">
                                    Mempelajadi berbagai macam hal yang berkaitan dengan masjid
                                    dan bagaimana cara memakmurkannya, banyak pelajaran mendasar
                                    berkaitan dengan menjadikan rumah Allah sebagai tempat
                                    peradaban islam.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="filterDiv season-1 p-1 rounded-0 border border-1 border-bottom-0 border-end-0 border-start-0">
                    <div class="sub-item-episode row g-0">
                        <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <h4 class="fw-semibold">8</h4>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center align-items-center mb-3 mb-md-0">
                            <img src="/image/kisah-nabi-adam.png" class="img-fluid rounded-start w-75" alt="..." />
                        </div>
                        <div class="col-md-8 d-flex justify-content-center align-items-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col col-9">
                                        <h5 class="fw-semibold">Kisah Nabi Luth</h5>
                                    </div>
                                    <div class="col col-3 text-end">
                                        <h5 class="fw-semibold">52m</h5>
                                    </div>
                                </div>
                                <p class="card-text justify-content-center">
                                    Mempelajadi berbagai macam hal yang berkaitan dengan masjid
                                    dan bagaimana cara memakmurkannya, banyak pelajaran mendasar
                                    berkaitan dengan menjadikan rumah Allah sebagai tempat
                                    peradaban islam.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="filterDiv season-1 p-1 rounded-0 border border-1 border-bottom-0 border-end-0 border-start-0">
                    <div class="sub-item-episode row g-0">
                        <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <h4 class="fw-semibold">9</h4>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center align-items-center mb-3 mb-md-0">
                            <img src="/image/kisah-nabi-adam.png" class="img-fluid rounded-start w-75" alt="..." />
                        </div>
                        <div class="col-md-8 d-flex justify-content-center align-items-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col col-9">
                                        <h5 class="fw-semibold">Kisah Nabi Ismail</h5>
                                    </div>
                                    <div class="col col-3 text-end">
                                        <h5 class="fw-semibold">52m</h5>
                                    </div>
                                </div>
                                <p class="card-text justify-content-center">
                                    Mempelajadi berbagai macam hal yang berkaitan dengan masjid
                                    dan bagaimana cara memakmurkannya, banyak pelajaran mendasar
                                    berkaitan dengan menjadikan rumah Allah sebagai tempat
                                    peradaban islam.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="filterDiv season-1 p-1 rounded-0 border border-1 border-bottom-0 border-end-0 border-start-0">
                    <div class="sub-item-episode row g-0">
                        <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <h4 class="fw-semibold">10</h4>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center align-items-center mb-3 mb-md-0">
                            <img src="/image/kisah-nabi-adam.png" class="img-fluid rounded-start w-75" alt="..." />
                        </div>
                        <div class="col-md-8 d-flex justify-content-center align-items-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col col-9">
                                        <h5 class="fw-semibold">Kisah Nabi Muhammad</h5>
                                    </div>
                                    <div class="col col-3 text-end">
                                        <h5 class="fw-semibold">52m</h5>
                                    </div>
                                </div>
                                <p class="card-text justify-content-center">
                                    Mempelajadi berbagai macam hal yang berkaitan dengan masjid
                                    dan bagaimana cara memakmurkannya, banyak pelajaran mendasar
                                    berkaitan dengan menjadikan rumah Allah sebagai tempat
                                    peradaban islam.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="filterDiv season-2 p-1 rounded-0 border border-1 border-bottom-0 border-end-0 border-start-0">
                    <div class="sub-item-episode row g-0">
                        <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <h4 class="fw-semibold">1</h4>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center align-items-center mb-3 mb-md-0">
                            <img src="/image/kisah-nabi-adam.png" class="img-fluid rounded-start w-75" alt="..." />
                        </div>
                        <div class="col-md-8 d-flex justify-content-center align-items-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col col-9">
                                        <h5 class="fw-semibold">Kisah Nabi Adam</h5>
                                    </div>
                                    <div class="col col-3 text-end">
                                        <h5 class="fw-semibold">52m</h5>
                                    </div>
                                </div>
                                <p class="card-text justify-content-center">
                                    Mempelajadi berbagai macam hal yang berkaitan dengan masjid
                                    dan bagaimana cara memakmurkannya, banyak pelajaran mendasar
                                    berkaitan dengan menjadikan rumah Allah sebagai tempat
                                    peradaban islam.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="filterDiv season-2 p-1 rounded-0 border border-1 border-bottom-0 border-end-0 border-start-0">
                    <div class="sub-item-episode row g-0">
                        <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <h4 class="fw-semibold">2</h4>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center align-items-center mb-3 mb-md-0">
                            <img src="/image/kisah-nabi-adam.png" class="img-fluid rounded-start w-75" alt="..." />
                        </div>
                        <div class="col-md-8 d-flex justify-content-center align-items-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col col-9">
                                        <h5 class="fw-semibold">Kisah Nabi Nuh</h5>
                                    </div>
                                    <div class="col col-3 text-end">
                                        <h5 class="fw-semibold">52m</h5>
                                    </div>
                                </div>
                                <p class="card-text justify-content-center">
                                    Mempelajadi berbagai macam hal yang berkaitan dengan masjid
                                    dan bagaimana cara memakmurkannya, banyak pelajaran mendasar
                                    berkaitan dengan menjadikan rumah Allah sebagai tempat
                                    peradaban islam.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="filterDiv season-2 p-1 rounded-0 border border-1 border-bottom-0 border-end-0 border-start-0">
                    <div class="sub-item-episode row g-0">
                        <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <h4 class="fw-semibold">3</h4>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center align-items-center mb-3 mb-md-0">
                            <img src="/image/kisah-nabi-adam.png" class="img-fluid rounded-start w-75" alt="..." />
                        </div>
                        <div class="col-md-8 d-flex justify-content-center align-items-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col col-9">
                                        <h5 class="fw-semibold">Kisah Nabi Luth</h5>
                                    </div>
                                    <div class="col col-3 text-end">
                                        <h5 class="fw-semibold">52m</h5>
                                    </div>
                                </div>
                                <p class="card-text">
                                    Mempelajadi berbagai macam hal yang berkaitan dengan masjid
                                    dan bagaimana cara memakmurkannya, banyak pelajaran mendasar
                                    berkaitan dengan menjadikan rumah Allah sebagai tempat
                                    peradaban islam.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="filterDiv season-2 p-1 rounded-0 border border-1 border-bottom-0 border-end-0 border-start-0">
                    <div class="sub-item-episode row g-0">
                        <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <h4 class="fw-semibold">4</h4>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center align-items-center mb-3 mb-md-0">
                            <img src="/image/kisah-nabi-isa.png" class="img-fluid rounded-start w-75" alt="..." />
                        </div>
                        <div class="col-md-8 d-flex justify-content-center align-items-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col col-9">
                                        <h5 class="fw-semibold">Kisah Nabi Isa</h5>
                                    </div>
                                    <div class="col col-3 text-end">
                                        <h5 class="fw-semibold">52m</h5>
                                    </div>
                                </div>
                                <p class="card-text">
                                    Mempelajadi berbagai macam hal yang berkaitan dengan masjid
                                    dan bagaimana cara memakmurkannya, banyak pelajaran mendasar
                                    berkaitan dengan menjadikan rumah Allah sebagai tempat
                                    peradaban islam.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="filterDiv season-3 p-1 rounded-0 border border-1 border-bottom-0 border-end-0 border-start-0">
                    <div class="sub-item-episode row g-0">
                        <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <h4 class="fw-semibold">1</h4>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center align-items-center mb-3 mb-md-0">
                            <img src="/image/kisah-nabi-nuh.png" class="img-fluid rounded-start w-75" alt="..." />
                        </div>
                        <div class="col-md-8 d-flex justify-content-center align-items-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col col-9">
                                        <h5 class="fw-semibold">Kisah Nabi Nuh</h5>
                                    </div>
                                    <div class="col col-3 text-end">
                                        <h5 class="fw-semibold">52m</h5>
                                    </div>
                                </div>
                                <p class="card-text">
                                    Mempelajadi berbagai macam hal yang berkaitan dengan masjid
                                    dan bagaimana cara memakmurkannya, banyak pelajaran mendasar
                                    berkaitan dengan menjadikan rumah Allah sebagai tempat
                                    peradaban islam.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="filterDiv season-3 p-1 rounded-0 border border-1 border-bottom-0 border-end-0 border-start-0">
                    <div class="sub-item-episode row g-0">
                        <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <h4 class="fw-semibold">2</h4>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center align-items-center mb-3 mb-md-0">
                            <img src="/image/kisah-nabi-nuh.png" class="img-fluid rounded-start w-75" alt="..." />
                        </div>
                        <div class="col-md-8 d-flex justify-content-center align-items-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col col-9">
                                        <h5 class="fw-semibold">Kisah Nabi Ibrahim</h5>
                                    </div>
                                    <div class="col col-3 text-end">
                                        <h5 class="fw-semibold">52m</h5>
                                    </div>
                                </div>
                                <p class="card-text">
                                    Mempelajadi berbagai macam hal yang berkaitan dengan masjid
                                    dan bagaimana cara memakmurkannya, banyak pelajaran mendasar
                                    berkaitan dengan menjadikan rumah Allah sebagai tempat
                                    peradaban islam.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="filterDiv season-4 p-1 rounded-0 border border-1 border-bottom-0 border-end-0 border-start-0">
                    <div class="sub-item-episode row g-0">
                        <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <h4 class="fw-semibold">1</h4>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center align-items-center mb-3 mb-md-0">
                            <img src="/image/kisah-nabi-nuh.png" class="img-fluid rounded-start w-75" alt="..." />
                        </div>
                        <div class="col-md-8 d-flex justify-content-center align-items-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col col-9">
                                        <h5 class="fw-semibold">Kisah Nabi Yusuf</h5>
                                    </div>
                                    <div class="col col-3 text-end">
                                        <h5 class="fw-semibold">52m</h5>
                                    </div>
                                </div>
                                <p class="card-text">
                                    Mempelajadi berbagai macam hal yang berkaitan dengan masjid
                                    dan bagaimana cara memakmurkannya, banyak pelajaran mendasar
                                    berkaitan dengan menjadikan rumah Allah sebagai tempat
                                    peradaban islam.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div style="margin-top: -15px">
                <h3 onclick="myFunction()" class="text-center">
                    <i class="fa-solid fa-circle-chevron-down"></i>
                </h3>
            </div>
            <div id="myDIV" class="mt-5">
                <div class="mb-4">
                    <h4 class="fw-bold">Trailer & Lainnya</h4>
                </div>
                <div class="row row-cols-1 row-cols-md-3 g-4">
                    <div class="col">
                        <div class="item-modal-footer card">
                            <img src="/image/kajian-4.png" class="card-img-top" alt="..." />
                            <div class="card-body">
                                <h6 class="card-title fw-bold">
                                    Trailer Kelas Kisah Nabi - Seasion 1
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="item-modal-footer card">
                            <img src="/image/kajian-3.png" class="card-img-top" alt="..." />
                            <div class="card-body">
                                <h6 class="card-title fw-bold">
                                    Trailer Kelas Kisah Nabi - Seasion 1
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="item-modal-footer card">
                            <img src="/image/kajian-4.png" class="card-img-top" alt="..." />
                            <div class="card-body">
                                <h6 class="card-title fw-bold">
                                    Trailer Kelas Kisah Nabi - Seasion 1
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Control-->
    <script>
        function myFunction() {
            var x = document.getElementById("myDIV");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }
    </script>

    <!-- FILTER SEASON -->
    <script>
        filterSelection("all");

        function filterSelection(c) {
            var x, i;
            x = document.getElementsByClassName("filterDiv");
            if (c == "all") c = "";
            for (i = 0; i < x.length; i++) {
                w3RemoveClass(x[i], "show");
                if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
            }
        }

        // Show filtered elements
        function w3AddClass(element, name) {
            var i, arr1, arr2;
            arr1 = element.className.split(" ");
            arr2 = name.split(" ");
            for (i = 0; i < arr2.length; i++) {
                if (arr1.indexOf(arr2[i]) == -1) {
                    element.className += " " + arr2[i];
                }
            }
        }

        // Hide elements that are not selected
        function w3RemoveClass(element, name) {
            var i, arr1, arr2;
            arr1 = element.className.split(" ");
            arr2 = name.split(" ");
            for (i = 0; i < arr2.length; i++) {
                while (arr1.indexOf(arr2[i]) > -1) {
                    arr1.splice(arr1.indexOf(arr2[i]), 1);
                }
            }
            element.className = arr1.join(" ");
        }

        // Add active class to the current control button (highlight it)
        var btnContainer = document.getElementById("myBtnContainer");
        var btns = btnContainer.getElementsByClassName("btn");
        for (var i = 0; i < btns.length; i++) {
            btns[i].addEventListener("click", function() {
                var current = document.getElementsByClassName("active");
                current[0].className = current[0].className.replace(" active", "");
                this.className += " active";
            });
        }
    </script>







    <script src="/bootstraplibrary/js/bootstrap.js"></script>
</body>

</html>
