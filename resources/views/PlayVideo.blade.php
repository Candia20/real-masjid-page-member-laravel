<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- CSS -->
    <!-- <link rel="stylesheet" href="assets/css/play-vidio.css" /> -->

    <!-- BOOTSTRAP CDN -->
    <!-- <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi"
      crossorigin="anonymous"
    /> -->

    <!-- CAROUSEL CDN -->
    <!-- <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
    />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css"
    /> -->

    <!-- RELOAD DATA DARI COMPONENTS -->
    <!-- <script src="https://code.jquery.com/jquery-1.10.2.js"></script> -->

    <!-- FONT -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@700&display=swap" rel="stylesheet" />

    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="/bootstraplibrary/css/bootstrap.css">

    <!-- CSS -->
    <link rel="stylesheet" href="/css/play-video.css">

    <!-- ICON -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" />


    <!-- RELOAD DATA DARI COMPONENTS -->
    <script src="/jquery-3.6.4/jquery.js"></script>

    <title>Play Vidio</title>

</head>

<body>
    <!-- RELOAD NAVBAR DARI COMPONENTS -->
    <div id="navbar-placeholder"></div>
    <script>
        $(function() {
            $("#navbar-placeholder").load("/components/navbar.html");
        });
    </script>

    <!-- Content Banner -->
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="/image/img-vidio-1.png" class="d-block w-100" alt="vidio-kisah-nabi-isa">
            </div>
        </div>
    </div>

    <!-- Real Holiday -->
    <section class="real-holiday">
        <div class="container-fluid">
            <hr />
            <div class="left">
                <p class="title-text fw-bold mb-2">Kisah Nabi</p>
                <p class="second-text fw-light">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, <br />
                    banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
            </div>
            <div class="right">
                <button type="button" class="btn" id="btn-circle-plus"><i class="fa fa-plus"></i></button>
                <!-- <div id="liveAlertPlaceholder"></div> -->
                <button type="button" class="btn" id="btn-circle">
                    <i class="fa fa-share-alt"></i>
                </button>

            </div>
            <hr />
        </div>
    </section>


    <!-- Seasion -->
    <section class="seasion">
        <div class="container-fluid">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link fw-bold active" id="seasion-1" data-bs-toggle="tab" data-bs-target="#home-tab-pane" type="button" role="tab" aria-controls="home-tab-pane" aria-selected="true">Seasion 1</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link fw-bold" id="seasion-2" data-bs-toggle="tab" data-bs-target="#profile-tab-pane" type="button" role="tab" aria-controls="profile-tab-pane" aria-selected="false">Seasion 2</button>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <!-- Seasion 1 -->
                <div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab" tabindex="0">
                    <section class="vidio">
                        <div class="row align-items-start ">
                            <div class="col">
                                <!-- <img src="assets/img/vidio-1.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/EMmPWZMQL68" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="https://www.youtube.com/watch?v=EMmPWZMQL68" class="fw-bold title-first">Kisah Nabi Adam - Episode 1</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                            <div class="col">
                                <!-- <img src="assets/img/vidio-2.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/EMmPWZMQL68" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Idris - Episode 2</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                            <div class="col">
                                <!-- <img src="assets/img/vidio-3.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/EMmPWZMQL68" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Ibrahim - Episode 3</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                        </div>
                        <div class="row align-items-center ">
                            <div class="col">
                                <!-- <img src="assets/img/vidio-4.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/EMmPWZMQL68" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Shaleh - Episode 4</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                            <div class="col">
                                <!-- <img src="assets/img/vidio-5.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/EMmPWZMQL68" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Hud - Episode 5</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                            <div class="col">
                                <!-- <img src="assets/img/vidio-6.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/EMmPWZMQL68" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Adam - Episode 6</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                        </div>
                        <div class="row align-items-center ">
                            <div class="col">
                                <!-- <img src="assets/img/vidio-7.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/EMmPWZMQL68" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Adam - Episode 7</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                            <div class="col">
                                <!-- <img src="assets/img/vidio-8.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/EMmPWZMQL68" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Adam - Episode 8</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                            <div class="col">
                                <!-- <img src="assets/img/vidio-9.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/EMmPWZMQL68" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Adam - Episode 9</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                        </div>
                        <div class="row align-items-end">
                            <div class="col">
                                <!-- <img src="assets/img/vidio-10.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/EMmPWZMQL68" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Adam - Episode 10</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                            <div class="col">
                                <!-- <img src="assets/img/vidio-11.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/EMmPWZMQL68" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Adam - Episode 11</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                            <div class="col">
                                <!-- <img src="assets/img/vidio-12.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/EMmPWZMQL68" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Adam - Episode 12</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                        </div>
                    </section>
                </div>

                <!-- Session 2 -->
                <div class="tab-pane fade" id="profile-tab-pane" role="tabpanel" aria-labelledby="profile-tab" tabindex="0">
                    <section class="vidio">
                        <div class="row align-items-start ">
                            <div class="col">
                                <!-- <img src="assets/img/vidio-1.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/COOboVlvMVs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="https://www.youtube.com/watch?v=COOboVlvMVs" class="fw-bold title-first">Kisah Nabi Adam - Episode 1</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                            <div class="col">
                                <!-- <img src="assets/img/vidio-2.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/COOboVlvMVs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Idris - Episode 2</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                            <div class="col">
                                <!-- <img src="assets/img/vidio-3.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/COOboVlvMVs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Ibrahim - Episode 3</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                        </div>
                        <div class="row align-items-center ">
                            <div class="col">
                                <!-- <img src="assets/img/vidio-4.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/COOboVlvMVs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Shaleh - Episode 4</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                            <div class="col">
                                <!-- <img src="assets/img/vidio-5.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/COOboVlvMVs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Hud - Episode 5</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                            <div class="col">
                                <!-- <img src="assets/img/vidio-6.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/COOboVlvMVs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Adam - Episode 6</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                        </div>
                        <div class="row align-items-center ">
                            <div class="col">
                                <!-- <img src="assets/img/vidio-7.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/COOboVlvMVs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Adam - Episode 7</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                            <div class="col">
                                <!-- <img src="assets/img/vidio-8.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/COOboVlvMVs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Adam - Episode 8</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                            <div class="col">
                                <!-- <img src="assets/img/vidio-9.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/COOboVlvMVs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Adam - Episode 9</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                        </div>
                        <div class="row align-items-end">
                            <div class="col">
                                <!-- <img src="assets/img/vidio-10.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/COOboVlvMVs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Adam - Episode 10</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                            <div class="col">
                                <!-- <img src="assets/img/vidio-11.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/COOboVlvMVs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Adam - Episode 11</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                            <div class="col">
                                <!-- <img src="assets/img/vidio-12.png" alt="" class="img-fluid" height="400px"> -->
                                <iframe height="190" width="340" src="https://www.youtube.com/embed/COOboVlvMVs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br />
                                <a href="" class="fw-bold title-first">Kisah Nabi Adam - Episode 12</a>
                                <p class="fw-light second-text-1">Mempelajadi berbagai macam hal yang berkaitan dengan masjid dan bagaimana cara memakmurkannya, banyak pelajaran mendasar berkaitan dengan menjadikan rumah Allah sebagai tempat peradaban islam.</p>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

        </div>
    </section>







    <!-- RELOAD FOOTER DARI COMPONENTS -->
    <div id="footer-placeholder"></div>
    <script>
        $(function() {
            $("#footer-placeholder").load("/components/footer.html");
        });

        document.getElementById('btn-circle-plus').onclick = function() {
            this.style.background = '#FBFB30';
        };

        // Alert
        const alertPlaceholder = document.getElementById('liveAlertPlaceholder')

        const alert = (message, type) => {
            const wrapper = document.createElement('div')
            wrapper.innerHTML = [
                `<div class="alert alert-${type} alert-dismissible" role="alert">`,
                `   <div>${message}</div>`,
                '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
                '</div>'
            ].join('')

            alertPlaceholder.append(wrapper)
        }

        const alertTrigger = document.getElementById('btn-circle')
        if (alertTrigger) {
            alertTrigger.addEventListener('click', () => {
                alert('Nice, you triggered this alert message!', 'success')
            })
        }
    </script>

    <!-- BOOTSTRAP CDN -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"></script>

    <!-- JQUERY CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <!-- CAROUSEL -->
    <!-- Carousel CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

    <!-- Popper -->
    <script>
        const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
        const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
    </script>


</body>

</html>